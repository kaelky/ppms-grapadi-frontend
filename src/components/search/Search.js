import React, { Component } from "react";
import Axios from "axios";

import "./Search.css";

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allProjects: [],
      query: "",
      filteredProjects: [],
    };

    this.handleQueryChange = this.handleQueryChange.bind(this);
  }

  async componentDidMount() {
    const response = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/projects/`);
    this.setState({
        allProjects: response.data
    });
  };

  handleFilterProjectAsQueryGiven = (query) => {
    const filteredProjects = this.state.allProjects.filter(
        project => 
            project.name.toLowerCase().includes(this.state.query.toLowerCase())
    );
    this.setState({
        filteredProjects
    });
  };

  handleQueryChange(event){
    this.setState({
        query: event.target.value
    })

    this.handleFilterProjectAsQueryGiven(this.state.query);
  };

  chooseProjectHandler = (projectId) => {
      window.location.replace(`/performance/${projectId}`);
  }

  render() {
        let placeholder = "Masukkan nama proyek..."
        if (this.props.searchPlaceholder !== undefined){
            placeholder = this.props.searchPlaceholder
        }

        if (this.props.showPerformanceSummary){
            return (
                <React.Fragment>
                    <div className = "search-input-show">
                        <input 
                            type = "text"
                            onChange = {this.handleQueryChange}
                            placeholder = {placeholder}
                        />
                    </div>
        
                    <div>
                        {this.state.filteredProjects.length > 0 && this.state.filteredProjects.map(project => (
                            <FilteredProjectsOption
                                key = {project.id} 
                                project = {project}
                                chooseProjectHandler = {() => {this.chooseProjectHandler(project.id)}}
                            />
                        ))}
                    </div>
                </React.Fragment>
            );
        }

        else {
            return (
                <React.Fragment>
                    <div className = "search-input">
                        <input 
                            type = "text"
                            onChange = {this.handleQueryChange}
                            placeholder = {placeholder}
                        />
                    </div>
        
                    <div>
                        {this.state.filteredProjects.length > 0 && this.state.filteredProjects.map(project => (
                            <FilteredProjectsOption
                                key = {project.id} 
                                project = {project}
                                chooseProjectHandler = {() => {this.chooseProjectHandler(project.id)}}
                                showPerformanceSummary = {this.props.showPerformanceSummary}
                            />
                        ))}
                    </div>
                </React.Fragment>
            );
        }
    };
}

const FilteredProjectsOption = (props) => {
    if (!props.showPerformanceSummary){
        return (
            <div className = "project-option-show" 
                onClick = {props.chooseProjectHandler}>
                {props.project.name}
            </div>
        )
    }
    else {
        return (
            <div className = "project-option-show" 
                onClick = {props.chooseProjectHandler}>
                {props.project.name}
            </div>
        )
    }
}
