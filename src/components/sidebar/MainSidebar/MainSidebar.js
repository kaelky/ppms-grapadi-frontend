import React, { Component } from "react";

import LogoIcon from "assets/Logo/logo.png";

import "./MainSidebar.css";

import SidebarMenu from "components/base/SidebarMenu/SidebarMenu.js";

export default class MainSidebar extends Component {
    constructor(props){
        super(props);
        this.state = {
            active: this.props.active,
        };
        this.changeActiveToDashboard = this.changeActiveToDashboard.bind(this);
        this.changeActiveToProjects = this.changeActiveToProjects.bind(this);
        this.changeActiveToEvents = this.changeActiveToEvents.bind(this);
    };

    changeActiveToDashboard(event){
        this.setState({
            active: "dashboard",
        })
        window.location.replace(`/performance`);
    }

    changeActiveToProjects(event){
        this.setState({
            active: "projects",
        })
        window.location.replace(`/projects`)
    }

    changeActiveToEvents(event){
        this.setState({
            active: "events",
        })
        window.location.replace(`/events`)
    }

    render(){
        return(
            <React.Fragment>
                <nav id = "sidebar">
                    {/* Logo dan Deskripsi Proyek */}
                    <div className = "logo-company-container">
                        <img className = "logo-company-image" src={LogoIcon} alt="logo" id="logo" />
                        <div className  = "logo-company-name-wrapper">
                            <span className = "logo-company-name">PT GRAPADI SEJAHTERA MANDIRI</span>
                            <span className = "logo-company-project-name">
                                PROJECT AND PERFORMANCE MANAGEMENT SYSTEM
                            </span>
                        </div>
                    </div>

                    {/* Filterisasi Role */}
                    <SidebarMenu
                        changeActiveToDashboard = {this.changeActiveToDashboard}
                        changeActiveToProjects = {this.changeActiveToProjects}
                        changeActiveToEvents = {this.changeActiveToEvents}
                        active = {this.state.active}
                    />
                    
                </nav>
            </React.Fragment>
        )
    }
}
