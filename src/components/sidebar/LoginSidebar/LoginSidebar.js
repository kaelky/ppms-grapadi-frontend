import React, { Component } from "react";

import UsernameIcon from "assets/LoginSidebar/user.png";
import PasswordIcon from "assets/LoginSidebar/key.png";
import swal from "sweetalert";

import "./LoginSidebar.css";

export default class LoginSidebar extends Component {
  constructor(props){
    super(props);
    this.state = {
      email   : "",
      password: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  };

  handleEmailChange = event => {
    this.setState({
      email: event.target.value,
    });
  };

  handlePasswordChange = event => {
    this.setState({
      password: event.target.value,
    });
  };  

  async handleSubmit(event){
    const response = await fetch(`https://ppms-backend-staging.herokuapp.com/api/login`, {
      method: 'POST',
      headers: {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json',
        'Access-Control-Allow-Origin' : '*'
      },
        body: JSON.stringify(this.state),
    }).then(response => response.json())

    if (response.response === "200") {
      localStorage.setItem("isLoggedOn", "true");
      localStorage.setItem("email", response.email);
      localStorage.setItem("role", response.role);
      localStorage.setItem("fullName", response.fullName);
      swal("Login berhasil!", {
        title: "SUKSES",
        icon: "success"
      })
        .then(() => {
            window.location.reload();
        });
    }

    else if (response.response === "Wrong Password"){
      // window.alert("Password Salah!");
      swal("Password Salah!", {
        title: "GAGAL",
        icon: "warning"
      })
        .then(() => {
            window.location.reload();
        });
      // window.location.reload();
    }

    else if (response.response === "404"){
      swal("Pengguna tidak ditemukan!", {
        title: "GAGAL",
        icon: "warning"
      })
        .then(() => {
            window.location.reload();
        });
      // window.location.reload();
    }    
  }

  render(){
    return(
      <React.Fragment>
        <div 
          className="login-form" 
        >
          <div className="login-form-group">
            <div className="login-form-group-icon-container">
              <img
                src={UsernameIcon}
                alt="username-icon"
                className="login-form-group-icon"
              />
            </div>
            <input 
              className="login-form-group-input" 
              placeholder="email" 
              type="text"
              name="email"
              onChange={this.handleEmailChange}
            />
          </div>
          <div className="login-form-group">
            <div className="login-form-group-icon-container">
              <img
                src={PasswordIcon}
                alt="password-icon"
                className="login-form-group-icon"
              />
            </div>
            <input
              className="login-form-group-input"
              placeholder="password"
              type="password"
              name="password"
              onChange={this.handlePasswordChange}
            />
          </div>
          <div className="login-button-container">
            <button className="custom-button" onClick={this.handleSubmit}>Login</button>
          </div>
        </div>
      </React.Fragment>
    )
  }
  
}
