import React, { Component } from "react";

import swal from "sweetalert";

import "./RegisterSidebar.css";

export default class RegisterAsManagerSidebar extends Component {
    constructor(props){
        super(props);
        this.state = {
            email   : "",
            password: "",
            nip     : "",
            fullName: "",
        };
        this.handleSubmitStaff = this.handleSubmitStaff.bind(this);
        this.handleSubmitManager = this.handleSubmitManager.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleNIPChange = this.handleNIPChange.bind(this);
        this.handleFullNameChange = this.handleFullNameChange.bind(this);
    };

  handleEmailChange = event => {
    this.setState({
      email: event.target.value,
    });
  };

  handlePasswordChange = event => {
    this.setState({
        password: event.target.value,
    });
  };
  
  handleNIPChange = event => {
    this.setState({
        nip: event.target.value,
    });
  };

  handleFullNameChange = event => {
    this.setState({
        fullName: event.target.value,
    });
  };

  changeToRegisterManager = event => {
    this.setState({

    })
  }

  async handleSubmitManager(event){
    const response = await fetch(`https://ppms-backend-staging.herokuapp.com/api/register/managers`, {
      method: 'POST',
      headers: {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json',
        'Access-Control-Allow-Origin' : '*'
      },
        body: JSON.stringify(this.state),
    })

    swal("Manajer baru berhasil dibuat!", {
        title: "SUKSES",
        icon: "success",
    }).then(() => {
        window.location.replace("/projects");
    });     
  }

  async handleSubmitStaff(event){
    const response = await fetch(`https://ppms-backend-staging.herokuapp.com/api/register/staffs/`, {
      method: 'POST',
      headers: {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json',
        'Access-Control-Allow-Origin' : '*'
      },
        body: JSON.stringify(this.state),
    })

    swal("Staf Konsultan baru berhasil dibuat!", {
        title: "SUKSES",
        icon: "success",
    }).then(() => {
        window.location.replace("/projects");
    });
  }

  render(){
    if (this.props.role === "Manajer"){
      return(
        <React.Fragment>
          <div 
            className="register-form" 
          >
          <div>
            <h4 className = "centered">Manajer</h4>
          </div>
            <div className="register-form-group">
              <input 
                className="register-form-group-input" 
                placeholder="Email" 
                type="text"
                name="email"
                onChange={this.handleEmailChange}
              />
            </div>
            <div className="register-form-group">
              <input
                className="register-form-group-input"
                placeholder="Password"
                type="password"
                name="password"
                onChange={this.handlePasswordChange}
              />
            </div>
            <div className="register-form-group">
              <input
                className="register-form-group-input"
                placeholder="NIP"
                type="text"
                name="nip"
                onChange={this.handleNIPChange}
              />
            </div>
            <div className="register-form-group">
              <input
                className="register-form-group-input"
                placeholder="Nama Lengkap"
                type="text"
                name="fullName"
                onChange={this.handleFullNameChange}
              />
            </div>
            <div className="register-button-container">
              <button className="custom-button" onClick={this.handleSubmitManager}>Submit</button>
            </div>
          </div>
        </React.Fragment>
      )
    }
   
    else if (this.props.role === "Staf"){
      return (
        <React.Fragment>
        <div 
          className="register-form" 
        >
          <div>
            <h4 className = "centered">Staf Konsultan</h4>
          </div>
          <div className="register-form-group">
            <input 
              className="register-form-group-input" 
              placeholder="Email" 
              type="text"
              name="email"
              onChange={this.handleEmailChange}
            />
          </div>
          <div className="register-form-group">
            <input
              className="register-form-group-input"
              placeholder="Password"
              type="password"
              name="password"
              onChange={this.handlePasswordChange}
            />
          </div>
          <div className="register-form-group">
            <input
              className="register-form-group-input"
              placeholder="NIP"
              type="text"
              name="nip"
              onChange={this.handleNIPChange}
            />
          </div>
          <div className="register-form-group">
            <input
              className="register-form-group-input"
              placeholder="Nama Lengkap"
              type="text"
              name="fullName"
              onChange={this.handleFullNameChange}
            />
          </div>
          <div className="register-button-container">
            <button className="custom-button" onClick={this.handleSubmitStaff}>Submit</button>
          </div>
        </div>
      </React.Fragment>
      )
    }

    else {
      return (
        <div>
          <div className="register-button-container">
            <button className="custom-button-manager" onClick={this.props.changeToRegisterManager()}>Manajer</button>
          </div>

          <div className="register-button-container">
            <button className="custom-button-staff" onClick={this.props.changeToRegisterStaff()}>Staf Konsultan</button>
          </div>
        </div>
      )
    }
  }
  
}
