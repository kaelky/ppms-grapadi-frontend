import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import { MDBDataTable } from 'mdbreact';
import { Component } from 'react';


export default class EventDatatable extends Component{
    constructor(props){
        super(props);
        this.columns=[
            {
            label: 'No.',
            field: 'number',
            sort: 'asc',
            width: 3
            },
            {
            label: 'Tanggal',
            field: 'tanggal',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Nama Acara',
            field: 'nama_acara',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Waktu',
            field: 'waktu',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Pembuat',
            field: 'pembuat',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Aksi',
            field: 'aksi',
            sort: 'asc',
            width: '20',
            }
        ]

    }
    render(){
        return (
            <MDBDataTable
                theadColor = "rgba-stylish-light"
                striped
                hover
                data={{
                    columns:this.columns,
                    rows:this.props.data.rows
                }}
            />
        );
    }
  
}