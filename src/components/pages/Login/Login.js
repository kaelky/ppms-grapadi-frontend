import React, { Component } from "react";

import LogoCompany from "./LogoCompany";

import "./Login.css";
import LoginSidebar from "components/sidebar/LoginSidebar/LoginSidebar.js";

export default class Login extends Component {
  render() {
    return (
      <div className="login-container">
        <div className="sidebar-container">
          <LogoCompany />
          <LoginSidebar />
          <div />
        </div>

        <div className="background-container">
          <img
            src="https://i.ibb.co/T4WPKR5/work.jpg"
            alt="back"
            className="background-img"
          />
        </div>
      </div>
    );
  }
}
