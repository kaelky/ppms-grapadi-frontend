import React from "react";

import LogoImgSrc from "assets/Logo/logo.png";
import "./LogoCompany.css";

const LogoCompany = props => (
  <div className="logo-company-container">
    <img className="logo-company-image" src={LogoImgSrc} alt="logo" id="logo" />
    <div className="logo-company-name-wrapper">
      <span className="logo-company-name">PT GRAPADI SEJAHTERA MANDIRI</span>
      <span className="logo-company-project-name">
        PROJECT AND PERFORMANCE MANAGEMENT SYSTEM
      </span>
    </div>
  </div>
);

export default LogoCompany;
