import React, { Component } from "react";
import Axios from "axios";
import ReactTable from "react-table";
import 'react-table/react-table.css';
import MuiDataTable from "mui-datatables";

import Search from "components/search/Search";
import PerformanceChart from "components/performance-chart/PerformanceChart.js";
import Header from "components/base/Header/Header";
import MainSidebar from "components/sidebar/MainSidebar/MainSidebar.js";

import "./Performance.css";

export default class Performance extends Component {
    constructor(props){
        super(props);
        this.state = {
            showPerformanceSummary: false,
            projectPerformanceSummaries: {},
            projectPerformanceChartSummaries: {},
            chosenProject: {}
        }; 
    };

    async componentDidMount(){
        if (this.props.match.params.projectId !== undefined){
            const projectPerformanceResponse = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/performance/summary/${this.props.match.params.projectId}/`);
            const projectPerformanceChartResponse = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/performance/chart/${this.props.match.params.projectId}/`);
            const projectResponse = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/projects/${this.props.match.params.projectId}/`);
            this.setState({
                showPerformanceSummary: true,
                projectPerformanceSummaries: projectPerformanceResponse.data,
                projectPerformanceChartSummaries: projectPerformanceChartResponse.data,
                chosenProject: projectResponse.data,
            });
            console.log(this.props.match.params.projectId);    
        }
        else {
            this.setState({
                showPerformanceSummary: false,
            });
        }
    };

    render() {
        if (this.state.showPerformanceSummary === false){
            return (
                <React.Fragment>
                    <div id = "wrapper">
                        <MainSidebar 
                            active = "dashboard"
                        />
                        <div id = "content">
                            <Header 
                                active = "dashboard"
                            />
                            <div className = "row">
                                <div className = "col-lg-6">
                                    <div className = "search-input">
                                        <Search 
                                            showPerformanceSummary = {this.state.showPerformanceSummary} 
                                        />
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            );
        }

        else {
            const columns = ["Id Staff", "Nama", "Role", "Jumlah Task Category", "Total Nilai"]

            const data = 
                this.state.projectPerformanceSummaries.map(
                    projectPerformanceSummary => (
                        [
                            projectPerformanceSummary.user.id,
                            projectPerformanceSummary.user.fullName,
                            projectPerformanceSummary.user.role.name,
                            projectPerformanceSummary.sumOfTaskCategories,
                            projectPerformanceSummary.averageOfTaskCategoriesScore,
                        ]
                    )
                )

            const options = {
                filterType: 'checkbox',
                responsive: 'scroll',
            }

            return (
                <React.Fragment>
                    <div id = "wrapper">
                        <MainSidebar 
                            active = "dashboard"
                        />
                        <div id = "content">
                            <Header 
                                active = "dashboard"
                                project = {this.state.chosenProject}
                            />
                            <Search
                                showPerformanceSummary = {this.state.showPerformanceSummary} 
                                searchPlaceholder = {this.state.chosenProject.name}
                            />
                            <PerformanceChart
                                chosenProject = {this.state.chosenProject}
                                projectPerformanceChartSummaries = {this.state.projectPerformanceChartSummaries}
                            />
                            <div className = "table-summary">
                                <MuiDataTable
                                    title = {"Rangkuman Performa Kerja Staf pada " + this.state.chosenProject.name}
                                    columns = {columns}
                                    data = {data}
                                    options = {options}
                                />
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            )
        }
    }
}