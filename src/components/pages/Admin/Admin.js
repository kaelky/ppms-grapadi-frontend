import React, {Component} from "react";
import MuiDataTable from "mui-datatables";
import './Admin.css';
import Axios from "axios";

export default class Admin extends Component {
    constructor(props){
        super(props);
        this.state = {
            users: [],
        }
    }
    
    async componentDidMount(){
        const usersResponse = await Axios.get(`https://ppms-backend.herokuapp.com/api/users/`);
        this.setState({
            users: usersResponse.data,
        });
    }

    render(){
        if (this.props.match.params.table == "users"){
            const columns = ["ID", "Email", "NIP", "Nama Lengkap", "Role", "Tanggal Dibuat"]

            const data = this.state.users.map(
                user => (
                    [
                        user.id,
                        user.email,
                        user.nip,
                        user.fullName,
                        user.role.name,
                        user.createdDate
                    ]
                )
            );

            const options = {
                filterType: 'checkbox',
                responsive: 'scroll',
                rowsPerPage: 5,
            }

            return (
                <React.Fragment>
                    <h1 className = "user-admin-title">USER</h1>
                    <hr/>
                    <div id = "admin-table">
                        <MuiDataTable
                            title = "Daftar Pengguna PPMS"
                            columns = {columns}
                            data = {data}
                            options = {options}
                        />
                    </div>
                </React.Fragment>
            )
        }
    }
}
