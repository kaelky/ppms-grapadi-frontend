import React, { Component } from "react";
import "./Milestone.css";
import FormModal from "components/forms/Modal/FormModal.js";
import FormUbahMilestone from "components/forms/FormMilestone/FormUbahMilestone";
import TaskCategoryContainer from "components/pages/ProjectDetail/TaskCategoryContainer/TaskCategoryContainer";
import {
  Row,
  Col,
  DropdownToggle,
} from "reactstrap";
import swal from 'sweetalert';

export default class Milestone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      startDate: "",
      endDate: "",
      dd1: false,
        modal1: false,
        modal2: false,
        modalEdit: false,
      
      
      
    };
   this.dropdownToggle = this.dropdownToggle.bind(this);
  
  }
  dropdownToggle() {
    this.setState({
      dd1: !this.state.dd1
    });
  }

  closeModal(tabId) {
    this.setState({
      [tabId]: false
    });
  }

  showModal(modal,myMilestone) {
    this.setState({
      [modal]: true
    });
    if(myMilestone!==null){
      this.setState({
        currMilestone: myMilestone,

      });
    }
   
  }


  async removeMilestoneHandler(event, milestoneId) {
    swal({
      title: "Apakah Kamu Yakin ingin menghapus?",
      text: "Setelah kamu mengapus data akan hilang dan tidak dapat di kembalikan!",
      icon: "warning",
      buttons: ["Tidak","Ya"],
      dangerMode: true,
    })
    .then((willDelete) =>{
      if(willDelete){
    fetch(`https://ppms-backend-staging.herokuapp.com/api/${milestoneId}/`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"

      }
    }).then(() => 
    { this.props.deleteFunction(milestoneId);
     });
     swal("Berhasil! Milestone " + this.props.milestoneSatuan.name + " berhasil di hapus")
     .then(
       function(){
         document.location.reload()}
     )
  }else{
    swal("Anda membatalkan penghapusan!");
  }
});
  }



    



  

  render() {
    if (localStorage.getItem("role") === "CEO" || localStorage.getItem("role") === "Manajer"){
      return (
        <div className="Milestone" data-toggle="collapse" data-target={"#milestone" + this.props.milestoneId}>
        <div className="row">
          
            <FormModal
              open={this.state.modal1}
              toggle={this.closeModal.bind(this, 'modal1')}
              judul="Ubah Milestone"
            >
              <FormUbahMilestone
                editMilestone={this.state.currMilestone}
                action="EDIT"
                idProyek={this.props.proyekId}
                milestoneId={this.props.milestoneId}
                toggle={this.closeModal.bind(this, 'modal1')}
              
              />
            </FormModal>
            </div>
  
            <div className="row" id="allMilestone">
            <div id="bx-mst" />
            <Row>
              <Col md={9}>
                <h6 id="mst-name">{this.props.milestoneSatuan.name}</h6>
                <p id="due-mst">End Date: {this.props.milestoneSatuan.endDate}</p>
              </Col>
              <Col md={1}>
                <button id ="ubah-milestone" type = "button" onClick={this.showModal.bind(this,'modal1', this.props.milestoneSatuan)}> Ubah </button>
              </Col>
              <Col md={1}>
                <button id = "hapus-milestone" type ="button"  onClick={event => this.removeMilestoneHandler(event, this.props.milestoneId)}> Hapus </button>          
              </Col>
              <Col md={1}>
  
            
            <DropdownToggle tag="a" className="nav-link" caret>
              Task Category
            </DropdownToggle>
            
          
              </Col>
            </Row>
            
          </div>
          
          <div id={"milestone" + this.props.milestoneId} class="collapse">
              <div className="barisTaskCategory">
                <TaskCategoryContainer  proyekId={this.props.proyekId} milestoneId={this.props.milestoneId}/>
      
              </div>
          </div>
        </div>
      );
    }
    
    else {
      return (
        <div className="Milestone" data-toggle="collapse" data-target={"#milestone" + this.props.milestoneId}>
        <div className="row">
          
            <FormModal
              open={this.state.modal1}
              toggle={this.closeModal.bind(this, 'modal1')}
              judul="Ubah Milestone"
            >
              <FormUbahMilestone
                editMilestone={this.state.currMilestone}
                action="EDIT"
                idProyek={this.props.proyekId}
                milestoneId={this.props.milestoneId}
                toggle={this.closeModal.bind(this, 'modal1')}
              
              />
            </FormModal>
            </div>
  
            <div className="row" id="allMilestone">
            <div id="bx-mst" />
            <Row>
              <Col md={9}>
                <h6 id="mst-name">{this.props.milestoneSatuan.name}</h6>
                <p id="due-mst">End Date: {this.props.milestoneSatuan.endDate}</p>
              </Col>
              <Col md={1}>
                <button id ="ubah-milestone" type = "button" onClick={() => swal(
                  'Staf tidak memiliki akses untuk mengubah Milestone!', {
                    icon: "error",
                    title: "GAGAL"
                  }
                )}> Ubah </button>
              </Col>
              <Col md={1}>
                <button id = "hapus-milestone" type ="button"  onClick={() => swal(
                  "Staf tidak memiliki akses untuk menghapus Milestone!", {
                    icon: "error",
                    title: "GAGAL"
                  }
                )}> Hapus </button>          
              </Col>
              <Col md={1}>
  
            
            <DropdownToggle tag="a" className="nav-link" caret>
              Task Category
            </DropdownToggle>
            
          
              </Col>
            </Row>
            
          </div>
          
          <div id={"milestone" + this.props.milestoneId} class="collapse">
              <div className="barisTaskCategory">
                <TaskCategoryContainer  proyekId={this.props.proyekId} milestoneId={this.props.milestoneId}/>
      
              </div>
          </div>
        </div>
      );
    }
    
  }

}
