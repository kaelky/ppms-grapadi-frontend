import React from 'react';
import { MDBDataTable } from 'mdbreact';
import { Component } from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

export default class UserDatatable extends Component{
    constructor(props){
        super(props);
        this.columns=[
            {
            label: 'Nama Anggota',
            field: 'nama_anggota',
            sort: 'asc',
            width: 150,
            }
        ]
    }
    
    render(){
        return (
            <MDBDataTable
                striped
                hover
                data={{
                    columns:this.columns,
                    rows:this.props.data.rows,
                }}
            />
        );
    }
}