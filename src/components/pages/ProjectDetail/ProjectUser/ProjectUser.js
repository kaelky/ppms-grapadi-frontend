import React, { Component } from 'react';
import MainSidebar from "components/sidebar/MainSidebar/MainSidebar.js";
import Header from "components/base/Header/Header";
import UserDatatable from 'components/pages/ProjectDetail/UserDataTable/UserDataTable';
import FormModal from "components/forms/Modal/FormModal.js";
import FormKomponenProyek from "components/forms/FormKomponenProyek/FormKomponenProyek"
import {
    Container,
} from "reactstrap";
import Axios from "axios";
import swal from 'sweetalert';
import './ProjectUser.css'
import { Col } from 'mdbreact';

class ProjectUser extends Component {
  constructor(props) {
    super(props);
    this.state={
        rows:[],
        users:[],
    dd1: false,
    modal1: false,
    modal2: false,
    modalEdit: false,
    namaUsers:"",
    user:"",
    project: {}

    };
    this.dropdownToggle = this.dropdownToggle.bind(this);

}
dropdownToggle() {
    this.setState({
      dd1: !this.state.dd1
    });
  }

  closeModal(tabId) {
    this.setState({
      [tabId]: false
    });
  }

  showModal(modal) {
    this.setState({
      [modal]: true
    });
  }



async componentDidMount() {
  
    await Axios.get(
      `https://ppms-backend-staging.herokuapp.com/api/projects/${this.props.match.params.id}`
    ).then(response => {
        
        const fetchResult = response.data;
        const pengguna = fetchResult.users;
        const namaPengguna = pengguna.fullName;
        var temp=[];
        for (var a in pengguna){
            temp.push(
                {
                nama_anggota: pengguna[a]["fullName"],
            });
        }
        this.setState({
          project: response.data,
        });  
        this.setState({
            rows: temp
        });
        this.setState({
            users: pengguna
        })
        this.setState({
            userNama: namaPengguna
        })
    });
  }

  async removeProjectHandler(event, userId) {
    swal({
      title: "Apakah Kamu Yakin ingin menghapus?",
      text: "Setelah kamu mengapus data akan hilang dan tidak dapat di kembalikan!",
      icon: "warning",
      buttons: ["Tidak","Ya"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if(willDelete){
        fetch(`https://ppms-backend-staging.herokuapp.com/api/project/deleteUser/${this.props.match.params.id}/${userId}`, {
          method: "DELETE",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
      }
    }).then(() => {
      let updatedKomponenProyek = [...this.state.users].filter(
        i => i.id !== userId
      );
      this.setState({
        pengguna:updatedKomponenProyek
      });
    });
    swal("Berhasil! Anggota telah berhasil dihapus dari proyek", "success")
      .then(
        function(){
          document.location.reload()}
      )
    
    
    }else{
      swal("Anda membatalkan anggota untuk dihapus!");
    }
    
  });
  
}


  

  render(){
    if (localStorage.getItem("role") === "CEO" || localStorage.getItem("role") === "Manajer") {
      return (
        <React.Fragment>
            <div id = "wrapper">
              <MainSidebar 
                active = "projects"
              />
              <div id = "content">
                <Header 
                  active = "projectDetail"
                  project = {this.state.project}
                  milestone = {this.state.milestone}
                  taskCategory = {this.state.taskCategory}
                />
                <Container>
                  <Col>
                    <div id = "content-users-project">
                      <button id = "content-users-button" type="button" onClick = {this.showModal.bind(this, 'modal1')}>Ubah Anggota</button>
                      <hr/>
                        <FormModal
                          open={this.state.modal1}
                          toggle={this.closeModal.bind(this, 'modal1')}
                          judul="Ubah Anggota"
                        >
                        <FormKomponenProyek 
                          action="CREATE"
                          id={this.props.match.params.id}
                          registeredUsers={this.state.users}
                          namaUsers={this.state.users.fullName}
                          toggle={this.closeModal.bind(this, 'modal1')}
                        />
                        </FormModal>
                      <UserDatatable data={{
                        rows: this.state.rows
                      }}/>
                      </div>
                  </Col>
                </Container>
              </div>
            </div>
          </React.Fragment>
      )
    }

    else {
      return (
        <React.Fragment>
            <div id = "wrapper">
              <MainSidebar 
                active = "projects"
              />
              <div id = "content">
                <Header 
                  active = "projectDetail"
                  project = {this.state.project}
                  milestone = {this.state.milestone}
                  taskCategory = {this.state.taskCategory}
                />
                <Container>
                  <Col>
                    <div id = "content-users-project">
                      <hr/>
                        <FormModal
                          open={this.state.modal1}
                          toggle={this.closeModal.bind(this, 'modal1')}
                          judul="Ubah Anggota"
                        >
                        <FormKomponenProyek 
                          action="CREATE"
                          id={this.props.match.params.id}
                          registeredUsers={this.state.users}
                          namaUsers={this.state.users.fullName}
                          toggle={this.closeModal.bind(this, 'modal1')}
                        />
                        </FormModal>
                      <UserDatatable data={{
                        rows: this.state.rows
                      }}/>
                      </div>
                  </Col>
                </Container>
              </div>
            </div>
          </React.Fragment>
      )
    }
}




}
export default ProjectUser;
