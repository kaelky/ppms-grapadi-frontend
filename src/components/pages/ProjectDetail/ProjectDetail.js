import React, { Component } from "react";
// import SideBar from '../../components/Sidebar/SideBar.js';
// import HeaderProjectDetail from '../../components/Headers/ProjectDetail/HeaderProjectDetail';
// import Role from '../../components/Headers/ProjectDetail/Role/Role.js';
import Axios from "axios";
import RealMilestoneContainer from "components/pages/ProjectDetail/RealMilestoneContainer/RealMilestoneContainer";
import "./ProjectDetail.css"
import { Link } from "react-router-dom";
import { Card } from "mdbreact";
import FormModal from "components/forms/Modal/FormModal";

import FormLabelStatus from "components/forms/FormLabelStatus/FormLabelStatus";
import FormAddLabel from "components/forms/FormAddLabel/FormAddLabel";
import {
  Container,
  Row,
  Col,
  Badge
} from "reactstrap";

import Header from "components/base/Header/Header";
import MainSidebar from "components/sidebar/MainSidebar/MainSidebar.js";
import swal from "sweetalert";


export default class ProjectDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      labelStatus: [],
      labelSatuan: null,
      projectId:'',
      project:{},
      pm:{},

      _labelNow: {},
      get labelNow() {
        return this._labelNow;
      },
      set labelNow(value) {
        this._labelNow = value;
      },
      dd1: false,
      modal1: false,
      modal2: false,
      modalEdit: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.dropdownToggle = this.dropdownToggle.bind(this);
    this.makeRupiah = this.makeRupiah.bind(this);
  }

  dropdownToggle() {
    this.setState({
      dd1: !this.state.dd1
    });
  }

  closeModal(tabId) {
    this.setState({
      [tabId]: false
    });
  }

  showModal(modal) {
    this.setState({
      [modal]: true
    });
    
  }

  async handleChange(event) {
  
    this.setState({
      labelSatuan: event.target.value
    });
  }

  async componentDidMount() {
    
    const { id } = this.props.match.params
    this.setState({
      projectId: id
    });

    await Axios.get(
      `https://ppms-backend-staging.herokuapp.com/api/projects/${id}`
    ).then(response => {
      
     
      const proyek = response.data;
      const pengguna = proyek.users;
      const label = proyek.stages;
      const labelSekarang = proyek.current_stage;
     
      this.setState({
        project: proyek
      });
      this.setState({
        pm: proyek.picuser
      });
      
      this.setState({
        users: pengguna
      });
      this.setState({
        labelStatus: label
      });
      this.setState({
        labelNow: labelSekarang
      });
    });
  }


  makeRupiah(nilai){
    var sumMoney = nilai + ''
    var sumMoneySplit = sumMoney.split('')
    var count = 0;
    var rupiah = '';
    var mataUang = 'Rp';
    var i;
    for (i=sumMoneySplit.length-1; i>=0; i--) {
      if (count == 3){
        rupiah += '.';
        rupiah += sumMoneySplit[i];
        count = 1;
      } else {
        rupiah += sumMoneySplit[i];
        count += 1;
      }
    }
    return mataUang + rupiah.split("").reverse().join("");
}
  

  render() {
    const { id } = this.state.projectId;
    
    if (localStorage.getItem("role") === "CEO" || localStorage.getItem("role") === "Manajer"){
      return (
        <React.Fragment>
        <div id = "wrapper">
          <MainSidebar 
                active = "projects"
          />
          <div id = "content">
            <Header 
                  active = "projectDetail"
                  project = {this.state.project}
                  milestone = {this.state.milestone}
                  taskCategory = {this.state.taskCategory}
            />
          </div>
            
            <div className="ProjectDetail">
            <Row className="rowBadge">
              <Badge color="success"className="badge" class="labelStatusBadge">{" "}
                {this.state.labelNow.name}{" "}
              </Badge>
            </Row >
                  
                <Row>
                  <Col md={0.2}>
                      <button id = "tambah-label" type = "button" onClick = {this.showModal.bind(this, "modal1")}>Tambah Label</button>
                  </Col>
                  
                  <Col>
                  <button id = "ubah-label" type = "button" onClick={this.showModal.bind(this, "modalEdit")}>Ubah Label</button>
                  </Col>
                </Row>
  
                <Row className="rowCard">
                  <Col sm={6}>
                    <div className="card" id="card-desc-prj"> 
                      <h5 id="desc-prj">
                        Deskripsi Proyek
                      </h5>
                      <p  id="deskripsi">
                        {this.state.project.description}
                      </p>
                    </div>
                  </Col>
  
                  <Col sm={3}>
                    <div className="card" id="card-prj-detail">
                      <Row>
                        <Col md={6}>
                          <span>Waktu Mulai</span>
                          <p>{this.state.project.startDate}</p> 
                        </Col>
                        
                        <Col md={6}>
                          <span>Project Manager</span>
                          <p>{this.state.pm.fullName}</p> 
                        </Col>
                      </Row>
                      <Row>
                      <Col md={6}>
                        <span>Tenggat Waktu</span>
                        <p>{this.state.project.endDate}</p> 
                        </Col>
                        <Col md={6}>
                        <span>Nama Klien</span>
                        <p>{this.state.project.client}</p> 
                        </Col>
                        </Row>
                        <Row>
                        <Col md={6}>
                        <span>Harga Proyek</span>
                        <p>{this.makeRupiah(this.state.project.value)}</p>
                        </Col>
                        </Row>
                      </div>
                    </Col>
  
                      <Col sm={2}>
                      
                        <Card className="card" id="card-anggota">
                            <h5 id="anggota-proyek">Anggota Proyek</h5>
                            <ul>
                              {this.state.users.slice(0,4).map(user => (
                                <div className="row">{user.fullName}</div>
                              ))}
                            </ul>
                            
                            
                              <div className="ubah-anggota">
                                <Link
                                  to={{
                                    pathname: `/projects/${
                                      this.props.match.params.id
                                    }/users`,
                                    state: { foo: this.state.users }
                                  }}
                                  activeClassName="active">
                                  Lihat Semua Anggota
                                </Link>
                              </div>
                          
                      </Card>
                      </Col>
  
                    </Row>
  
              
  
              <div className="container">
                <div className="row">
                  <div className="col-md-2">
              
                    <Container>
                      <FormModal
                        open={this.state.modal1}
                        toggle={this.closeModal.bind(this, "modal1")}
                        judul="Tambah Label Status"
                      >
                        <FormLabelStatus
                          action="CREATE"
                          id={this.props.match.params.id}
                          labelStatus={this.state.labelStatus}
                          toggle={this.closeModal.bind(this, "modal1")}
                        />
                      </FormModal>
                      <FormModal
                        open={this.state.modalEdit}
                        toggle={this.closeModal.bind(this, "modalEdit")}
                        judul="Ubah Label Status"
                      >
                        <FormAddLabel
                          id={this.props.match.params.id}
                          editLabel={this.state.labelNow}
                          labelStatus={this.state.labelStatus}
                          action="EDIT"
                          toggle={this.closeModal.bind(this, "modalEdit")}
                        />
  
                      </FormModal>
                      
                    </Container>
                </div>
              
                  <div className="row">
                    <div className="col-md-4"></div>
                  </div>
                </div>
              </div>
                
                <div className="row">
                      <div className="col-md-5"></div>
                      <div className="col-md-4"></div>
                      <div className="col-md-2"></div>
                </div>
                
                <div className="row">
                    <div className="col-md-8">
                      <RealMilestoneContainer
                        id={this.props.match.params.id}
                      />
                    </div>
                </div>
                </div>
          </div>
        </React.Fragment>
      );
    }

      else {
        return (
          <React.Fragment>
          <div id = "wrapper">
            <MainSidebar 
                  active = "projects"
            />
            <div id = "content">
              <Header 
                    active = "projectDetail"
                    project = {this.state.project}
                    milestone = {this.state.milestone}
                    taskCategory = {this.state.taskCategory}
              />
            </div>
              
              <div className="ProjectDetail">
              <Row className="rowBadge">
                <Badge color="success"className="badge" class="labelStatusBadge">{" "}
                  {this.state.labelNow.name}{" "}
                </Badge>
              </Row >
                    
                  <Row>
                    <Col md={0.2}>
                        <button id = "tambah-label" type = "button" onClick = {() => swal(
                          "Staf tidak memiliki akses untuk menambah label!", {
                            icon: "error",
                            title: "GAGAL"
                          }
                        )}>Tambah Label</button>
                    </Col>
                    
                    <Col>
                    <button id = "ubah-label" type = "button" onClick={() => swal(
                      "Staf tidak memiliki akses untuk mengubah label!", {
                        icon: "error",
                        title: "GAGAL"
                      }
                    )}>Ubah Label</button>
                    </Col>
                  </Row>
    
                  <Row className="rowCard">
                    <Col sm={6}>
                      <div className="card" id="card-desc-prj"> 
                        <h5 id="desc-prj">
                          Deskripsi Proyek
                        </h5>
                        <p  id="deskripsi">
                          {this.state.project.description}
                        </p>
                      </div>
                    </Col>
    
                    <Col sm={3}>
                      <div className="card" id="card-prj-detail">
                        <Row>
                          <Col md={6}>
                            <span>Waktu Mulai</span>
                            <p>{this.state.project.startDate}</p> 
                          </Col>
                          
                          <Col md={6}>
                            <span>Project Manager</span>
                            <p>{this.state.pm.fullName}</p> 
                          </Col>
                        </Row>
                        <Row>
                        <Col md={6}>
                          <span>Tenggat Waktu</span>
                          <p>{this.state.project.endDate}</p> 
                          </Col>
                          <Col md={6}>
                          <span>Nama Klien</span>
                          <p>{this.state.project.client}</p> 
                          </Col>
                          </Row>
                          <Row>
                          <Col md={6}>
                          <span>Harga Proyek</span>
                          <p>{this.makeRupiah(this.state.project.value)}</p>
                          </Col>
                          </Row>
                        </div>
                      </Col>
    
                        <Col sm={2}>
                        
                          <Card className="card" id="card-anggota">
                              <h5 id="anggota-proyek">Anggota Proyek</h5>
                              <ul>
                                {this.state.users.slice(0,4).map(user => (
                                  <div className="row">{user.fullName}</div>
                                ))}
                              </ul>
                              
                              
                                <div className="ubah-anggota">
                                  <Link
                                    to={{
                                      pathname: `/projects/${
                                        this.props.match.params.id
                                      }/users`,
                                      state: { foo: this.state.users }
                                    }}
                                    activeClassName="active">
                                    Lihat Semua Anggota
                                  </Link>
                                </div>
                            
                        </Card>
                        </Col>
    
                      </Row>
    
                
    
                <div className="container">
                  <div className="row">
                    <div className="col-md-2">
                
                      <Container>
                        <FormModal
                          open={this.state.modal1}
                          toggle={this.closeModal.bind(this, "modal1")}
                          judul="Tambah Label Status"
                        >
                          <FormLabelStatus
                            action="CREATE"
                            id={this.props.match.params.id}
                            labelStatus={this.state.labelStatus}
                            toggle={this.closeModal.bind(this, "modal1")}
                          />
                        </FormModal>
                        <FormModal
                          open={this.state.modalEdit}
                          toggle={this.closeModal.bind(this, "modalEdit")}
                          judul="Ubah Label Status"
                        >
                          <FormAddLabel
                            id={this.props.match.params.id}
                            editLabel={this.state.labelNow}
                            labelStatus={this.state.labelStatus}
                            action="EDIT"
                            toggle={this.closeModal.bind(this, "modalEdit")}
                          />
    
                        </FormModal>
                        
                      </Container>
                  </div>
                
                    <div className="row">
                      <div className="col-md-4"></div>
                    </div>
                  </div>
                </div>
                  
                  <div className="row">
                        <div className="col-md-5"></div>
                        <div className="col-md-4"></div>
                        <div className="col-md-2"></div>
                  </div>
                  
                  <div className="row">
                      <div className="col-md-8">
                        <RealMilestoneContainer
                          id={this.props.match.params.id}
                        />
                      </div>
                  </div>
                  </div>
            </div>
          </React.Fragment>
        );   
      }
    }
}
