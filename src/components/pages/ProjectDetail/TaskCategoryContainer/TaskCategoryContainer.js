import FormModal from "components/forms/Modal/FormModal";
import React, { Component } from "react";
import axios from "axios";
import TaskCategory from 'components/pages/ProjectDetail/TaskCategory/TaskCategory';
import FormTaskCategory from "components/forms/FormTaskCategory/FormTaskCategory";
import "./TaskCategoryContainer.css";

import {
    Row,
    Col,
  } from "reactstrap";
  import swal from 'sweetalert';
  

class TaskCategoryContainer extends Component {
  constructor(props){
    super(props)
    this.state = {
        taskCategories: [],
        dd1: false,
        modal1: false,
        modal2: false,
        modalEdit: false,
        currTaskCategory: '',
      };
    
    this.showModal = this.showModal.bind(this);
}

dropdownToggle() {
    this.setState({
      dd1: !this.state.dd1
    });
  }

  closeModal(tabId) {
    this.setState({
      [tabId]: false
    });
  }

  showModal(modal,myTaskCategory) {
    this.setState({
      [modal]: true
    });
    if(myTaskCategory!==null){
      this.setState({
        currTaskCategory: myTaskCategory
      }); 
    } 
  }

 async componentDidMount(){
    
    await axios.get(`https://ppms-backend-staging.herokuapp.com/api/milestones/${this.props.milestoneId}/taskCategories/`)
    .then(response => {
        const fetchResult = response.data;
       
        
        this.setState({
            taskCategories: fetchResult
        })
    })
  }

  async removeTaskCategoryHandler(event, taskCategoryId) {

      swal({
        title: "Apakah kamu yakin?",
        text: "Sekali kamu menghapus maka data akan hilang semuanya!",
        icon: "warning",
        buttons: ["Tidak", "Ya"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          fetch(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${taskCategoryId}`, {
            method: "DELETE",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            }
          }).then(() => {
            let updatedTaskCategories = [...this.state.taskCategories].filter(
              i => i.id !== taskCategoryId
            );
            this.setState({
              taskCategories:updatedTaskCategories
            });
          });
          swal("Sukses! Task Category telah dihapus!", {
            icon: "success",
          });
        } else {
          swal("Task Category tidak jadi dihapus");
        }
      });
        
      }

  render() {
    if (localStorage.getItem("role") === "CEO" || localStorage.getItem("role") === "Manajer"){
      return ( 
        <div id="allTaskCategory">
            <FormModal
                open={this.state.modal1}
                toggle={this.closeModal.bind(this, 'modal1')}
                judul="Tambah Task Category"
            >
                <FormTaskCategory
                    action="CREATE"
                    proyekId={this.props.proyekId}
                    milestoneId={this.props.milestoneId}
                    toggle={this.closeModal.bind(this, 'modal1')}
                
                />
            </FormModal>

            <FormModal
                    open={this.state.modalEdit}
                    toggle={this.closeModal.bind(this, 'modalEdit')}
                    
                    judul="Ubah Task Category"      
                 >
                  <FormTaskCategory 
                    editTaskCategory={this.state.currTaskCategory} 
                    action="EDIT" 
                    toggle={this.closeModal.bind(this, 'modalEdit')}
                    proyekId={this.props.proyekId}
                    milestoneId={this.props.milestoneId}/>
            </FormModal>

            <h5 className="tambahTask" onClick={this.showModal.bind(this, "modal1")}> Tambah Task Category +</h5>
            {this.state.taskCategories.map(
                        taskCategory => 
                                <Row >
                                    <Col md={9}>
                                        <TaskCategory                           
                                            taskCategoryObj = {taskCategory}
                                            proyekId={this.props.proyekId}
                                        />
                                    </Col >
                                    <Col md={3}>
                                      <span id="ubah-tc" onClick={this.showModal.bind(this,'modalEdit', taskCategory )}> Ubah </span>
                                      <span id="hapus-tc" onClick={event => this.removeTaskCategoryHandler(event, taskCategory.id)}> Hapus </span>
                                    </Col>
                                </ Row>
                    )}
        </div>
    );}

    else {
      return ( 
        <div id="allTaskCategory">
            <FormModal
                open={this.state.modal1}
                toggle={this.closeModal.bind(this, 'modal1')}
                judul="Tambah Task Category"
            >
                <FormTaskCategory
                    action="CREATE"
                    proyekId={this.props.proyekId}
                    milestoneId={this.props.milestoneId}
                    toggle={this.closeModal.bind(this, 'modal1')}
                
                />
            </FormModal>

            <FormModal
                    open={this.state.modalEdit}
                    toggle={this.closeModal.bind(this, 'modalEdit')}
                    
                    judul="Ubah Task Category"      
                 >
                  <FormTaskCategory 
                    editTaskCategory={this.state.currTaskCategory} 
                    action="EDIT" 
                    toggle={this.closeModal.bind(this, 'modalEdit')}
                    proyekId={this.props.proyekId}
                    milestoneId={this.props.milestoneId}/>
            </FormModal>

            <h5 className="tambahTask" onClick={() => swal(
              "Staf tidak memiliki akses untuk menambah Task Category!", {
                icon: "error",
                title: "GAGAL"
              }
            )}> Tambah Task Category +</h5>
            {this.state.taskCategories.map(
                        taskCategory => 
                                <Row >
                                    <Col md={9}>
                                        <TaskCategory                           
                                            taskCategoryObj = {taskCategory}
                                            proyekId={this.props.proyekId}
                                        />
                                    </Col >
                                    <Col md={3}>
                                      <span id="ubah-tc" onClick={() => swal(
                                        "Staf tidak memiliki akses untuk mengubah Task Category!", {
                                          icon: "error",
                                          title: "GAGAL"
                                        }
                                      )}> Ubah </span>
                                      <span id="hapus-tc" onClick={() => swal(
                                        "Staf tidak memiliki akses untuk menghapus Task Category!", {
                                          icon: "error",
                                          title: "GAGAL"
                                        }
                                      )}> Hapus </span>
                                    </Col>
                                </ Row>
                    )}
        </div>
    );}
    }
}

export default TaskCategoryContainer;
