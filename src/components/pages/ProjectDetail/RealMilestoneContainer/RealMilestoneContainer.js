import React, { Component } from "react";
import axios from "axios";
import Milestone from "components/pages/ProjectDetail/Milestone/Milestone";
import FormMilestone from "components/forms/FormMilestone/FormMilestone";
import FormModal from "components/forms/Modal/FormModal";
import FormUbahMilestone from "components/forms/FormMilestone/FormUbahMilestone";
import "./RealMilestoneContainer.css";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  FormFeedback,
  FormText,
  Badge
} from "reactstrap";
import swal from "sweetalert";


export default class RealMilestoneContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      milestones: [],
      dd1: false,
      modal1: false,
      modal2: false,
      modalEdit: false,
      milestoneSatuan:""

    };
   
    this.dropdownToggle = this.dropdownToggle.bind(this);
    this.deleteFunction = this.deleteFunction.bind(this);
  }
  dropdownToggle() {
    this.setState({
      dd1: !this.state.dd1
    });
  }

  closeModal(tabId) {
    this.setState({
      [tabId]: false
    });
  }

  showModal(modal) {
    this.setState({
      [modal]: true
    });
  
  }

  componentDidMount(){
      axios
      .get(`https://ppms-backend-staging.herokuapp.com/api/${this.props.id}/milestones/`)
      .then(response => {
        const fetchResult = response.data;
        this.setState({
          milestones: fetchResult
        });
      });

  }

  
   deleteFunction(milestoneId){
   let updatedMilestones = [...this.state.milestones].filter(
     i => i.id !== milestoneId
  );
    this.setState({
     milestones: updatedMilestones
   });
 }
  

  

  render() {
    if (localStorage.getItem("role") === "CEO" || localStorage.getItem("role") === "Manajer"){
      return (
        <Container>
          <Row>
          <Row className="tambah">
            <div className="create-mst" 
            onClick={this.showModal.bind(this, "modal1")}> 
            <h2 className="milestone-title">Tambah Milestone +</h2>
           </div>  
          </Row>
  
          <Row className="rowMilestone">
           <h3 id="milestone">Milestone</h3>
            <ul>
            {this.state.milestones.map(milestone => (
                  <div className="row">
                    <Milestone
                      milestoneId={milestone.id}
                      milestoneSatuan={milestone}
                      deleteFunction={this.deleteFunction}
                      proyekId={this.props.id}
                      
                    />
                  </div>
                
              ))}
  
            </ul>
         </Row>
            <FormModal
              open={this.state.modal1}
              toggle={this.closeModal.bind(this, 'modal1')}
              judul="Tambah Milestone"
            >
              <FormMilestone
                action="CREATE"
                id={this.props.id}
                toggle={this.closeModal.bind(this, 'modal1')}
                // milestoneId={this.props.id}
              
              />
            </FormModal>
         </Row>
        </Container>
      );
    }
    
    else {
      return (
        <Container>
          <Row>
          <Row className="tambah">
            <div className="create-mst" 
            onClick={() => swal(
              'Staf tidak memiliki akses untuk menambah milestone!', {
                icon: "error",
                title: "GAGAL"
              }
            )}> 
            <h2 className="milestone-title">Tambah Milestone +</h2>
           </div>  
          </Row>
  
          <Row className="rowMilestone">
           <h3 id="milestone">Milestone</h3>
            <ul>
            {this.state.milestones.map(milestone => (
                  <div className="row">
                    <Milestone
                      milestoneId={milestone.id}
                      milestoneSatuan={milestone}
                      deleteFunction={this.deleteFunction}
                      proyekId={this.props.id}
                      
                    />
                  </div>
                
              ))}
  
            </ul>
         </Row>
            <FormModal
              open={this.state.modal1}
              toggle={this.closeModal.bind(this, 'modal1')}
              judul="Tambah Milestone"
            >
              <FormMilestone
                action="CREATE"
                id={this.props.id}
                toggle={this.closeModal.bind(this, 'modal1')}
                // milestoneId={this.props.id}
              
              />
            </FormModal>
         </Row>
        </Container>
      );
    }
  }

}








