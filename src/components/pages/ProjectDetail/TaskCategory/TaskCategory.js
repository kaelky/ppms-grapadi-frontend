import React, { Component } from 'react';
import "./TaskCategory.css";
import {Link} from "react-router-dom";

export default class TaskCategory extends Component {
    constructor(props){
        super(props);
        this.state = {

        };
    }
    render(){
        return(
            <div>
                <Link
                    to={{
                        pathname: `/taskCategories/${
                        this.props.taskCategoryObj.id
                    }`}}
                    activeClassName="active">
                    <div id = "taskCategoryDesc">
                        <h6 id = "taskCategory" className="taskCategory">{this.props.taskCategoryObj.name}</h6> 
                    </div>
                </Link>
                <h6 id="dueDate-tc"><span id = "blue">Due Date: </span>{this.props.taskCategoryObj.dueDate}</h6>
                {/* <h6 className="taskCategory"><a href = "/taskCategories/"{this.props.taskCategoryObj.id}></a></h6> */}
            </div>
        );
    }
}