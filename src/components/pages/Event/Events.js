import React, { Component } from "react";
import './Event.css';

import axios from  'axios';
import swal from 'sweetalert';
import MainSidebar from "components/sidebar/MainSidebar/MainSidebar.js";
import Header from "components/base/Header/Header";
import FormModal from "components/forms/Modal/FormModal";
import FormEvent from "components/forms/FormEvent/FormEvent";
import EventDatatable from "components/table/EventDataTable";

import { Button, Container } from 'reactstrap';
import { Row , Col } from "reactstrap";

const monthsArray = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
                "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

var getDateString = function(date){
    var dateSplit = date.split("-");
    var month = dateSplit[1][1];
    var dateString = dateSplit[2] +" "+monthsArray[month-1]+" "+dateSplit[0];
    return dateString;
}

export default class Event extends Component {
      constructor(props) {
      super(props);
      this.state={
            rows:[],
            events: [],
            currEvent: '',
            modal1: false,
      };   
      }

      closeModal(tabId){
            this.setState({
                [tabId]: false
            });
        }
    
        showDelete(modal, myEvent){
            this.removeEventHandler(myEvent);
        }
    
        showModal(modal, myEvent) {
            this.setState({
                [modal]: true
            });
            if(myEvent!==null){
                this.setState({
                    currEvent: myEvent
                });
            }
        }
    
    
        componentDidMount() {
            axios
            .get('https://ppms-backend-staging.herokuapp.com/api/events/')
            .then(response => {
                  const fetchResult = response.data;
                  var temp=[];
                  var number = 1;
                  if (localStorage.getItem("role") === "CEO" || localStorage.getItem("role") === "Manajer"){
                        for (var a in fetchResult) {
                              temp.push({
                                    number: ''+number,
                                    tanggal: fetchResult[a]["date"],
                                    nama_acara: fetchResult[a]["name"],
                                    waktu: fetchResult[a]["time"],
                                    pembuat: fetchResult[a]["author"],
                                    aksi:  
                                          <ul>
                                                <button id = "button-biru" onClick={this.showModal.bind(this, 'modalEdit', fetchResult[a])}> Ubah </button>   
                                                <button id = "button-merah" onClick={this.showDelete.bind(this, 'modalDelete', fetchResult[a])}> Hapus </button>
                                          </ul>
                                    }
                              );
                              number++
                        }
                        this.setState({
                              rows:temp,
                              events:fetchResult
                        });
                  }
                  else {
                        for (var a in fetchResult) {
                              temp.push({
                                    number: ''+number,
                                    tanggal: fetchResult[a]["date"],
                                    nama_acara: fetchResult[a]["name"],
                                    waktu: fetchResult[a]["time"],
                                    pembuat: fetchResult[a]["author"],
                                    aksi:  
                                          <ul>
                                                <button id = "button-biru" onClick={() => swal(
                                                      "Staf tidak memiliki akses untuk mengubah jadwal acara!", {
                                                            icon: "error",
                                                            title: "GAGAL"
                                                      }
                                                )}> Ubah </button>   
                                                <button id = "button-merah" onClick={() => swal(
                                                      "Staf tidak memiliki akses untuk menghapus jadwal acara!", {
                                                            icon: "error",
                                                            title: "GAGAL"
                                                      }
                                                )}> Hapus </button>
                                          </ul>
                                    }
                              );
                              number++
                        }
                        this.setState({
                              rows:temp,
                              events:fetchResult
                        });   
                  }
            });
      }
    
        async removeEventHandler(eventNow){
            swal({
                title: "Apakah anda yakin akan menghapus acara "+eventNow.name+ " pada "+ getDateString(eventNow.date)+"?",
                icon: "warning",
                buttons: ["Tidak", "Ya"],
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  fetch(`https://ppms-backend-staging.herokuapp.com/api/events/${eventNow.id}`, {
                        
                    method: "DELETE",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                  })
                  swal("SUKSES", "Acara "+ eventNow.name+ " berhasil dihapus", {
                    icon: "success",
                  })
                  .then(
                        function(){
                            document.location.reload()}
                  );
                } 
              });
        }
    


      render(){
            if  (localStorage.getItem("role") === "CEO" || localStorage.getItem("role") === "Manajer"){
                  return (
                        <React.Fragment>
                              <div id = "wrapper">
                                    <MainSidebar 
                                          active = "events"
                                    />
                                    <div id = "content">
                                          <Header 
                                                active = "events"
                                          />
                                          <div>
                                          
                                          <Container>
                                                      <Col>
                                                            <div id = "content-event">
      
                                                            <button id = "button-tambah-event" onClick={this.showModal.bind(this, 'modal1')} >
                                                            Tambah Acara +
                                                            </button>
                                                            <hr/>
      
                                                            <Container>
                                                                  <FormModal
                                                                        open={this.state.modal1}
                                                                        toggle={this.closeModal.bind(this, 'modal1')}
                                                                        judul="Tambah Acara">
                                                                        <FormEvent 
                                                                              editEvent={this.state.currEvent}
                                                                              action="CREATE" togel={this.closeModal.bind(this, 'modal1')}/>    
                                                                  </FormModal>
      
                                                                  <FormModal
                                                                        open={this.state.modalEdit}
                                                                        toggle={this.closeModal.bind(this, 'modalEdit')}
                                                                        judul="Ubah Acara">
                                                                        <FormEvent 
                                                                              editEvent={this.state.currEvent} 
                                                                              action="UPDATE" togel={this.closeModal.bind(this, 'modalEdit')}/>
                                                                  </FormModal>
                                                            </Container>
                                                            <EventDatatable data={{
                                                                  rows: this.state.rows
                                                            }}/>
                                                            </div>
                                                      </Col>
                                          </Container>
                                    </div>
                              </div>
                        </div>
                  </React.Fragment>
                  )
            }
            else {
                  return (
                        <React.Fragment>
                              <div id = "wrapper">
                                    <MainSidebar 
                                          active = "events"
                                    />
                                    <div id = "content">
                                          <Header 
                                                active = "events"
                                          />
                                          <div>
                                          
                                          <Container>
                                                      <Col>
                                                            <div id = "content-event">
                                                                  <EventDatatable data={{
                                                                        rows: this.state.rows
                                                                  }}/>
                                                            </div>
                                                      </Col>
                                          </Container>
                                    </div>
                              </div>
                        </div>
                  </React.Fragment>
                  )
            }
      }
}