import React, { Component } from 'react';
//import SideBar from '../../components/Base/SideBar/SideBar.js'
import ProjectDataTable from "components/pages/Projects/ProjectDataTable/ProjectDataTable";
import {BrowserRouter, Link} from 'react-router-dom';
import MainSidebar from "components/sidebar/MainSidebar/MainSidebar.js";
import Header from "components/base/Header/Header";

import axios from "axios";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col
  } from "reactstrap";
import FormModal from "components/forms/Modal/FormModal";
import FormProject from "components/forms/FormProject/FormProject";
import swal from 'sweetalert';
import "./Projects.css";


var compare_dates = function(date1,date2){
    console.log("---- DATE -----")
    
    var first = new Date(date1);
    var second = new Date(date2);
      if (first>second)
           return (1);
      else if (first<second) 
          return (-1);
      else 
          return (0); 
  }
class Projects extends Component {
  constructor(props) {
    super(props);
    this.state={
        rows:[],
        projects: [],
        currProject: '',
        dd1: false,
        modal1: false,
        modal2: false,
        modalEdit: false,
        modalHapus: false
    };
    this.dropdownToggle = this.dropdownToggle.bind(this);
    
}

dropdownToggle() {
    this.setState({
        dd1: !this.state.dd1
    });
}

closeModal(tabId) {
    this.setState({
        [tabId]: false
    });
}

 

showModal(modal,myProject) {
    this.setState({
      [modal]: true
    });
    if(myProject!==null){
      this.setState({
        currProject: myProject
    });
       }
}

showDelete(modal, myProject){
    
    this.removeProjectHandler(myProject);
}

  


componentDidMount() {
  if (localStorage.getItem("role") === "CEO"){
    axios
    .get(`https://ppms-backend-staging.herokuapp.com/api/projects/`)
    .then(response => {
    const fetchResult = response.data;
    var temp=[];
    var number = 1;
    for (var project in fetchResult) {
        temp.push(
            {
                number: ''+number,
                nama_proyek: 
                <h6 className="redirect-title">
                <Link to={{
                  pathname:`/projects/`+ fetchResult[project]["id"],
                  projectProps:{
                    id: fetchResult[project]["id"] 
                  }
                }}     >
                {fetchResult[project]["name"]}
                </Link>
                </h6>,

               
                tenggat_waktu: fetchResult[project]["endDate"],
                nama_klien: fetchResult[project]["client"],
                nilai_proyek: this.makeRupiah(fetchResult[project]["value"]),
                status_proyek: this.setStatusProyek(fetchResult[project]),
                aksi:  <ul>
                            <button color="primary" id = "button-biru" className="button-biru" onClick={this.showModal.bind(this,'modalEdit',fetchResult[project])}> Ubah </button>
                            <button color="danger" id = "button-merah" onClick={this.showDelete.bind(this, 'modalDelete', fetchResult[project])}> Hapus </button>
                       </ul>
            }
        );
        number++
    }
    this.setState({
        rows:temp,
        projects:fetchResult
    });
    });
  }

  else {
    if (localStorage.getItem("role") === "Manajer" || localStorage.getItem("role") === "Staf Konsultan"){
        axios
          .get(`https://ppms-backend-staging.herokuapp.com/api/users/${localStorage.getItem("email")}/projects`)
            .then(response => {
              const fetchResult = response.data;
              var temp=[];
              var number = 1;

              if (localStorage.getItem("role") === "Manajer"){
                for (var project in fetchResult) {
                  temp.push({
                    number: ''+number,
                    nama_proyek: 
                    <h6 className="redirect-title">
                    <Link to={{
                      pathname:`/projects/`+ fetchResult[project]["id"],
                      projectProps:{
                        id: fetchResult[project]["id"] 
                      }
                    }}     >
                    {fetchResult[project]["name"]}
                    </Link>
                    </h6>,
                    tenggat_waktu: fetchResult[project]["endDate"],
                    nama_klien: fetchResult[project]["client"],
                    status_proyek: this.setStatusProyek(fetchResult[project]),
                    aksi: 
                      <ul>
                        <button color="primary" id = "button-biru" className="button-biru" onClick={this.showModal.bind(this,'modalEdit',fetchResult[project])}> Ubah </button>
                        <button color="danger" id = "button-merah" onClick={this.showDelete.bind(this, 'modalDelete', fetchResult[project])}> Hapus </button>
                      </ul>
                  });
                  number++
              };}
              else if (localStorage.getItem("role") === "Staf Konsultan"){
                for (var project in fetchResult) {
                  temp.push({
                    number: ''+number,
                    nama_proyek: 
                    <h6 className="redirect-title">
                    <Link to={{
                      pathname:`/projects/`+ fetchResult[project]["id"],
                      projectProps:{
                        id: fetchResult[project]["id"] 
                      }
                    }}     >
                    {fetchResult[project]["name"]}
                    </Link>
                    </h6>,
                    tenggat_waktu: fetchResult[project]["endDate"],
                    nama_klien: fetchResult[project]["client"],
                    status_proyek: this.setStatusProyek(fetchResult[project]),
                    aksi: 
                      <ul>
                        <button color="primary" id = "button-biru" className="button-biru" onClick={() => swal(
                          "Staf tidak memiliki akses untuk mengubah proyek!", {
                            icon : "error",
                            title: "GAGAL"
                          }
                        )}> Ubah </button>
                        <button color="danger" id = "button-merah" onClick={() => swal(
                          "Staf tidak memiliki akses untuk menghapus proyek!", {
                            icon: "error",
                            title: "GAGAL"
                          }
                        )}> Hapus </button>
                      </ul>
                  });
                  number++
              }
            };
              this.setState({
                rows:temp,
                projects:fetchResult
              });
            });
          }
        }
      }

makeRupiah(nilai){
    var sumMoney = nilai + ''
    var sumMoneySplit = sumMoney.split('')
    var count = 0;
    var rupiah = '';
    var mataUang = 'Rp';
    var i;
    for (i=sumMoneySplit.length-1; i>=0; i--) {
      if (count == 3){
        rupiah += '.';
        rupiah += sumMoneySplit[i];
        count = 1;
      } else {
        rupiah += sumMoneySplit[i];
        count += 1;
      }
    }
    return mataUang + rupiah.split("").reverse().join("");
}

setStatusProyek(project) {
    if (compare_dates(project["endDate"],new Date()) < 0) {
        return 'Proyek Selesai'
    }
    else if (compare_dates(project["endDate"],new Date()) > 0) {
        return 'Proyek Berjalan'
    }
    return 'gaada'
}

async removeProjectHandler(project) {
    swal({
        title: "Apakah kamu yakin?",
        text: "Setelah kamu menghapus, data tidak dapat dilakukan pemulihan!",
        icon: "warning",
        buttons: ["Tidak", "Ya"],
        dangerMode: true,
      })
    .then((willDelete) => {
        if (willDelete) {
          fetch(`https://ppms-backend-staging.herokuapp.com/api/projects/${project.id}`, {
            method: "DELETE",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            }
          })
          swal("Sukses! Proyek telah dihapus", {
            icon: "success",
          }).then(
            function(){
                document.location.reload()}
          );
        } else {
          swal("Batal menghapus!");
        }
    });   
}


render(){
  if (localStorage.getItem("role") === "Staf Konsultan"){
    return (
      <React.Fragment>
      <div id = "wrapper">
          <MainSidebar 
              active = "projects"
          />
          <div id = "content">
            <Header 
              active = "projects"
            />
            
        <Container>
            <Col lg={12}>
              <div id = "content-projects">
            <hr/>
            <ProjectDataTable 
              data={{
                rows: this.state.rows
              }}
              role={localStorage.getItem("role")}
            />
              </div>
            </Col> 
        </Container>
        </div>
      </div>
      </React.Fragment>
    )
  }

  else {
    return (
        <React.Fragment>
        <div id = "wrapper">
            <MainSidebar 
                active = "projects"
            />
            <div id = "content">
              <Header 
                active = "projects"
              />
              
          <Container>
              <Col lg={12}>
                <div id = "content-projects">
              <button id = "button-tambah-proyek" onClick={this.showModal.bind(this, 'modal1')}>
                  Tambah Proyek +
              </button>

              <hr/>
              <Container>
                  <FormModal
                      open={this.state.modal1}
                      toggle={this.closeModal.bind(this, 'modal1')}
                      judul="Tambah Proyek"
                  >
                      <FormProject action="CREATE"  toggle={this.closeModal.bind(this, 'modal1')}/>
                  </FormModal>

                  <FormModal
                      open={this.state.modalEdit}
                      toggle={this.closeModal.bind(this, 'modalEdit')}
                      judul="Ubah Proyek"      
                  >
                      <FormProject editProject={this.state.currProject} action="EDIT" toggle={this.closeModal.bind(this, 'modalEdit')}/>
                  </FormModal>               
              </Container>
              <ProjectDataTable 
                data={{
                  rows: this.state.rows
                }}
                role={localStorage.getItem("role")}
              />
                </div>
              </Col> 
          </Container>
          </div>
        </div>
        </React.Fragment>
    )
  }
}
}

export default Projects;
