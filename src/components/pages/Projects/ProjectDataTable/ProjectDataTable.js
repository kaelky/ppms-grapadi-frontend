import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import { MDBDataTable } from 'mdbreact';
import { Component } from 'react';


export default class ProjectDataTable extends Component{
    constructor(props){
        super(props);
        this.state={
            columns:
            [
            {
            label: '#',
            field: 'number',
            sort: 'asc',
            width: 3
            },
            {
            label: 'Nama Proyek',
            field: 'nama_proyek',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Tenggat Waktu',
            field: 'tenggat_waktu',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Nama Klien',
            field: 'nama_klien',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Nilai Proyek',
            field: 'Nilai_proyek',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Status Proyek',
            field: 'status_proyek',
            sort: 'asc',
            width: 150
            },
            {
            label: 'Aksi',
            field: 'aksi',
            sort: 'asc',
            width: 200
            }
        ]
        }
    }

    componentDidMount() {
        if ((this.props.role == "Staf Konsultan") || (this.props.role == "Manajer")){
            this.setState({
                columns:
                [
                    {
                    label: '#',
                    field: 'number',
                    sort: 'asc',
                    width: 3
                    },
                    {
                    label: 'Nama Proyek',
                    field: 'nama_proyek',
                    sort: 'asc',
                    width: 150
                    },
                    {
                    label: 'Tenggat Waktu',
                    field: 'tenggat_waktu',
                    sort: 'asc',
                    width: 150
                    },
                    {
                    label: 'Nama Klien',
                    field: 'nama_klien',
                    sort: 'asc',
                    width: 150
                    },
                    {
                    label: 'Status Proyek',
                    field: 'status_proyek',
                    sort: 'asc',
                    width: 150
                    },
                    {
                    label: 'Aksi',
                    field: 'aksi',
                    sort: 'asc',
                    width: 200
                    }
                ] 
            })
        }
    }

    render(){
        return (
            <MDBDataTable
                theadColor = "rgba-stylish-light"
              striped
              hover
              data={{
                  columns:this.state.columns,
                  rows:this.props.data.rows
              }}
            />
          );
    }
  
}