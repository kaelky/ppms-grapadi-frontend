import React, { Component } from "react";

import LogoCompany from "components/pages/Login/LogoCompany";

import "./Register.css";
import RegisterSidebar from "components/sidebar/RegisterSidebar/RegisterSidebar.js";

export default class Register extends Component {
    constructor(props){
        super(props);
        this.state = {
            role: "none",
        };
        this.changeToRegisterManager = this.changeToRegisterManager.bind(this);
        this.changeToRegisterStaff = this.changeToRegisterStaff.bind(this);
    }

    changeToRegisterManager() {
        this.setState({
            role: "Manajer"
        });
    };

    changeToRegisterStaff(){
        this.setState({
            role: "Staf"
        })
    }
    
  render() {
    return (
      <div className="login-container">
        <div className="sidebar-container">
          <LogoCompany />
          <RegisterSidebar 
            role = {this.state.role}
            changeToRegisterManager = {() => this.changeToRegisterManager}
            changeToRegisterStaff = {() => this.changeToRegisterStaff}
          />
          <div />
        </div>

        <div className="background-container">
          <img
            src="https://i.ibb.co/T4WPKR5/work.jpg"
            alt="back"
            className="background-img"
          />
        </div>
      </div>
    );
  }
}
