import React, { Component } from "react";

import "./Comments.css";
import FormComment from 'components/forms/FormComment/FormComment';
import Comment from 'components/pages/TaskCategoryDetail/Comments/Comment/Comment';
import Axios from 'axios';
import swal from 'sweetalert';

export default class Comments extends Component {
    constructor(props){
        super(props);
        this.state = {
            comments: [],
            toBeUpdatedComment: null,
            modalUpdateComment: false, 
        };
        this.showModal = this.showModal.bind(this);
        this.removeCommentHandler = this.removeCommentHandler.bind(this);
    }

    closeModal(stateIndex){
        this.setState({
            [stateIndex]: false
        });
    }

    showModal(modal, toBeUpdatedComment){
        this.setState({
            [modal]: true,
        });
        if (toBeUpdatedComment !== null){
            this.setState({
                toBeUpdatedComment
            })
        }
    }

    async componentDidMount(){
        const commentsResponse = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${this.props.taskCategoryId}/comments`)
        this.setState({
            comments: commentsResponse.data,
        });
    };

    async removeCommentHandler(event, commentId){
        swal({
            title: "Apakah Anda yakin akan menghapus komentar?",
            text: "Anda tidak dapat membatalkan penghapusan ini!",
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if(willDelete) {
                    fetch(`https://ppms-backend-staging.herokuapp.com/api/comments/${commentId}`, {
                        method: "DELETE",
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json"
                        }
                    })
                        .then(() => {
                            let updatedComments = [...this.state.comments].filter(
                                i => i.id !== commentId
                            );
                            this.setState({
                                tasks: updatedComments,
                            });
                        });
                        swal({
                            title: "SUKSES",
                            text: "Komentar berhasil dihapus",
                            icon: "success",
                            button: "Lanjutkan",
                        })
                            .then(() => {
                                document.location.reload();
                            });
                }
                else {
                    swal("Hapus komentar berhasil dibatalkan");
                };
            });
    }

    render() {
        if ((localStorage.getItem("role") === "Staf Konsultan") || (localStorage.getItem("role") === "Manajer")) {
            return (
                <React.Fragment>
                    <div className="Comments">
                        <div>
                            <FormComment
                                action = "CREATE"
                                taskCategoryId = {this.props.taskCategoryId}/>
                        </div>
                        <div className="allComments">
                            {this.state.comments.map(
                                comment =>
                                    <Comment
                                        comment = {comment}
                                        userName = {comment.user.fullName}
                                        userRole = {comment.user.role.name}
                                        userEmail = {comment.user.email}
                                        commentId = {comment.id}
                                        content = {comment.content}
                                        removeCommentHandler = {this.removeCommentHandler}
                                        createdDate = {comment.createdDate}
                                        updatedDate = {comment.updatedDate}
                                                                                />
                            )}
                        </div>
                    </div>
                </React.Fragment>
            )
        } 
        
        else {
            return (
                <React.Fragment>
                    <div className="CommentsCEO">
                        <h3 id="mnj-ceo">Dari Manajer</h3>
                        <hr id="linemnj-ceo"/>
                        <div className="allComments">
                            {this.state.comments.map(
                                comment =>
                                    <Comment
                                        comment = {comment}
                                        userName = {comment.user.fullName}
                                        userRole = {comment.user.role.name}
                                        userEmail = {comment.user.email}
                                        commentId = {comment.id}
                                        content = {comment.content}
                                        createdDate = {comment.createdDate}
                                        updatedDate = {comment.updatedDate}
                                        />
                            )}
                        </div>
                    </div>
                </React.Fragment>
            )
        }
    }
}
