import React, { Component } from 'react';
import Moment from 'react-moment';

import "./Comment.css";
import FormModal from 'components/forms/Modal/FormModal';
import FormCommentEdit from 'components/forms/FormCommentEdit/FormCommentEdit';

export default class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toBeUpdatedComment: null,
            modalUpdateComment: false,
        };
        this.showModal = this.showModal.bind(this);
        this.userRole = this.props.userRole;
        this.userName = this.props.userName;
        this.userEmail = this.props.userEmail;
    };

    closeModal(stateIndex){
        this.setState({
            [stateIndex]: false
        });
    }

    showModal(modal, toBeUpdatedComment){
        this.setState({
            [modal]: true,
        });
        if (toBeUpdatedComment !== null){
            this.setState({
                toBeUpdatedComment
            })
        }
    }

    render() {
        const create = new Date(this.props.createdDate);
        const update = new Date(this.props.updatedDate);

        if ((this.userRole === "Manajer") && (localStorage.getItem("email") === this.userEmail)) {
            return (
                <React.Fragment>
                    <div className="card-header" id="byManager">
                        {this.props.userName} - {this.props.userRole}
                    </div>
                    <div className="card-body">
                        <p id="komen">{this.props.content}</p>
                        <p id="cmt-dt"> <strong>Dibuat: </strong> 
                            <Moment date={create} format="DD-MM-YYYY HH:MM" />
                                <strong> Diubah: </strong>
                            <Moment date={update} format="DD-MM-YYYY HH:MM" />
                        </p>
                        <div id="comment-detail">
                            <a className="col md-3" onClick={this.showModal.bind(this, 'modalUpdateComment', this.props.comment)} id="edit-comment">Ubah</a>
                            <a className="col md-3" onClick={event => this.props.removeCommentHandler(event, this.props.commentId)} id="hapus-comment">Hapus</a>
                        </div>

                        <FormModal
                            open = {this.state.modalUpdateComment}
                            toggle = {this.closeModal.bind(this, 'modalUpdateComment')}
                            judul = "Ubah Komentar"
                        >
                            <FormCommentEdit action = "EDIT" toBeUpdatedComment = {this.state.toBeUpdatedComment} toggle = {this.closeModal.bind(this, 'modalUpdateComment')} />
                        </FormModal>
                    </div>
                </React.Fragment>
            )
        }

        if (this.userRole === "Manajer") {
            return (
                <React.Fragment>
                    <div className="card-header" id="byManager">
                        {this.props.userName} - {this.props.userRole}
                    </div>
                    <div className="card-body">
                        <p id="komen">{this.props.content}</p>
                        <p id="cmt-dt"> <strong>Dibuat: </strong> 
                            <Moment date={create} format="DD-MM-YYYY HH:MM" />
                                <strong> Diubah: </strong>
                            <Moment date={update} format="DD-MM-YYYY HH:MM" />
                        </p>
                    </div>
                </React.Fragment>
            )
        }

        if ((this.userRole === "Staf Konsultan") && (localStorage.getItem("email") === this.userEmail)) {
            return (
                <React.Fragment>
                    <div className="card-header" id="byStaff">
                        {this.props.userName} - {this.props.userRole}
                    </div>
                    <div className="card-body">
                        <p id="komen">{this.props.content}</p>
                        <p id="cmt-dt"> <strong>Dibuat: </strong> 
                            <Moment date={create} format="DD-MM-YYYY HH:MM" />
                             <strong> Diubah: </strong>
                             <Moment date={update} format="DD-MM-YYYY HH:MM" />
                        </p>
                        <div id="comment-detail">
                            <a className="col md-3" onClick={this.showModal.bind(this, 'modalUpdateComment', this.props.comment)} id="edit-comment">Ubah</a>
                            <a className="col md-3" onClick={event => this.props.removeCommentHandler(event, this.props.commentId)} id="hapus-comment">Hapus</a>
                        </div>
                    
                        <FormModal
                            open = {this.state.modalUpdateComment}
                            toggle = {this.closeModal.bind(this, 'modalUpdateComment')}
                            judul = "Ubah Komentar"
                        >
                            <FormCommentEdit action = "EDIT" toBeUpdatedComment = {this.state.toBeUpdatedComment} toggle = {this.closeModal.bind(this, 'modalUpdateComment')} />
                        </FormModal>

                    </div>
                </React.Fragment>
            )
        }

        if (this.userRole === "Staf Konsultan") {
            return (
                <React.Fragment>
                    <div className="card-header" id="byStaff">
                        {this.props.userName} - {this.props.userRole}
                    </div>
                    <div className="card-body">
                        <p id="komen">{this.props.content}</p>
                        <p id="cmt-dt"> <strong>Dibuat: </strong> 
                            <Moment date={create} format="DD-MM-YYYY HH:MM" />
                                <strong> Diubah: </strong>
                            <Moment date={update} format="DD-MM-YYYY HH:MM" />
                        </p>
                    </div>
                </React.Fragment>
            )
        }

        else {
            if (this.userRole === "Manajer") {
                return (
                    <React.Fragment>
                        <div className="card-header" id="byManager">
                            {this.props.userName} - {this.props.userRole}
                        </div>
                        <div className="card-body">
                            <p id="komen">{this.props.content}</p>
                            <p id="cmt-dt"> <strong>Dibuat: </strong> 
                                <Moment date={create} format="DD-MM-YYYY HH:MM" />
                                    <strong> Diubah: </strong>
                                <Moment date={update} format="DD-MM-YYYY HH:MM" />
                            </p>
                        </div>
                    </React.Fragment>
                )
            }
    
            if (this.userRole === "Staf Konsultan") {
                return (
                    <React.Fragment>
                        <div className="card-header" id="byStaff">
                            {this.props.userName} - {this.props.userRole}
                        </div>
                        <div className="card-body">
                            <p id="komen">{this.props.content}</p>
                            <p id="cmt-dt"> <strong>Dibuat: </strong> 
                                <Moment date={create} format="DD-MM-YYYY HH:MM" />
                                    <strong> Diubah: </strong>
                                <Moment date={update} format="DD-MM-YYYY HH:MM" />
                            </p>
                        </div>
                    </React.Fragment>
                )
            }
        }
    }
}
