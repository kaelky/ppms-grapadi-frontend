import React, { Component } from 'react';
import axios from "axios";
import './DocumentList.css';


export default class DocumentList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listicle: [],
            documents: []
          };
      }

      componentDidMount() {
        axios
        .get(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${this.props.taskCategoryId}/documents`)
        .then(response => {
          console.log("MASUKK")
          const fetchResult = response.data;
          var temp=[];
          for (var document in fetchResult) {
            temp.push(
              {
                id: fetchResult[document]["id"],
                name: fetchResult[document]["name"],
                resourceAddress: fetchResult[document]["resourceAddress"]
              }
            )
          }
          this.setState({
            listicle:temp,
            documents:fetchResult
          })
          console.log(this.state.listicle)
        });
      }

      render() {
          return (
            <div className="Documents">
                {this.state.documents.map(
                    document =>
                    <li>
                        <a href={document.resourceAddress}>{document.name}</a>
                        
                        <div id="hapus">
                            <a onClick={event => this.props.removeDocumentHandler(event, document.id)}>x</a>
                        </div>
                    </li>
                )}
            </div>
          );
      }
}
