import React, { Component } from 'react';
import Axios from 'axios';
import './Submissions.css';
import swal from 'sweetalert';
import DocumentList from './DocumentList.js';

export default class Submission extends Component {
    constructor(props){
        super(props);
        this.state = {
            body: {
                name: "",
                fileUrl: "",
                submissionId: ""
            },
            documents: []
        }
        this.uploadDocument = this.uploadDocument.bind(this)
        this.setDocumentPostBody = this.setDocumentPostBody.bind(this);
        this.removeDocumentHandler = this.removeDocumentHandler.bind(this);
    }

    componentDidMount(){
        Axios.get(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${this.props.taskCategoryId}/documents`)
            .then(response => {
                this.setState({
                    documents: response.data
                })
            })
    }

    setDocumentPostBody(data){
        this.setState({
            body: data
        });
        console.log(this.state.body);
    }

    async uploadAWS(){
        console.log("upload to AWS")

    }

    async uploadDocument(event){
        let file = event.target.files[0];
        console.log("UPLOADING...")

        if (file) {
            let data = new FormData();
            data.append('file', file);

            const responseUpload = await Axios.post(`https://ppms-backend-staging.herokuapp.com/storage/uploadFile`, data)
            let url = responseUpload.data.resourceAddress
            let nama = responseUpload.data.name
        
        Axios.post(`https://ppms-backend-staging.herokuapp.com/api/documents/${this.props.taskCategoryId}/`, {
            resourceAddress: url,
            name: nama
        })
        .then(function (response) {
            console.log(response);
            swal({
                title: "SUKSES",
                text: "Dokumen berhasil diunggah",
                icon: "success",
                button : "Lanjutkan",
            })
            .then(() => {
                document.location.reload();
            });
          })
          .catch(function (error) {
            console.log(error);
            swal({
                title: "GAGAL",
                text: "Dokumen gagal diunggah",
                icon: "error",
                button: "Lanjutkan",
            });
          });
        }
    }

    async removeDocumentHandler(event, documentId) {
        swal({
            title: "Apakah Anda yakin akan menghapus dokumen?",
            text: "Anda tidak dapat membatalkan penghapusan ini",
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                fetch(`https://ppms-backend-staging.herokuapp.com/api/documents/${documentId}`, {
                method: 'DELETE',
                headers: {
                    'Accept' : 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                }
            })
            .then(() => {
                let updatedDocuments = [...this.state.documents].filter(
                    i => i.id !== documentId
                );
                this.setState({
                    documents: updatedDocuments,
                });
            });
            swal({
                    title: "SUKSES",
                    text: "Dokumen berhasil dihapus",
                    icon: "success",
                    button: "Lanjutkan",
            })
                .then(() => {
                    document.location.reload();
                });
            }

            else {
                swal("Hapus dokumen berhasil dibatalkan");
            };
        });
    }

    render(){
        if (localStorage.getItem("role") === "CEO"){
            return (
                <div className="Submission">
                    <h3 id="file-sub">File Submission</h3>
                    <hr id="line-sub"></hr>
                    <div id="sub-box">
                        <input 
                            id = "file-upload"
                            type="file"
                            name="myFile"
                            onChange={this.uploadDocument} />
                    </div>
                    <DocumentList 
                        taskCategoryId={this.props.taskCategoryId}
                        removeDocumentHandler = {this.removeDocumentHandler}
                    />
                </div>
            )
        }
        else {
            return (
                <div className="SubmissionExceptCEO">
                    <h3 id="file-sub">File Submission</h3>
                    <hr id="line-sub"></hr>
                    <div id="sub-box">
                        <input 
                            id = "file-upload"
                            type="file"
                            name="myFile"
                            onChange={this.uploadDocument} />
                    </div>
                    <DocumentList 
                        taskCategoryId={this.props.taskCategoryId}
                        removeDocumentHandler = {this.removeDocumentHandler}
                    />
                </div>
            )
        }
    }
}
