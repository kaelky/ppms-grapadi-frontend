import React, {Component} from "react";
import Axios from "axios";

import "./Feedback.css";

import FormModal from "components/forms/Modal/FormModal";
import FormScoring from "components/forms/FormScoring/FormScoring";

export default class Feedback extends Component {
    constructor(props){
        super(props);
        this.state = {
            submission: {},
            modalEditSubmissionElements: false,
        }
    };

    async componentDidMount(){
        const submissionElementsResponse = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${this.props.taskCategoryId}/submissions/`)
        this.setState({
            submission: submissionElementsResponse.data,
        });
        console.log(this.state.submission);
    };

    closeModal(stateIndex){
        this.setState({
            [stateIndex]: false
        });
    }

    showModal(modal, toBeUpdatedTask){
        this.setState({
            [modal]: true,
        });
        if (toBeUpdatedTask !== null){
            this.setState({
                toBeUpdatedTask
            });
        }
    }

    render(){
        if (localStorage.getItem("role") === "CEO"){
            return (
                <React.Fragment>
                    <div className="feedbackContainer">
                        <h3 id="fb-ceo">Dari CEO</h3>
                        <hr id="line-ceo"/>
                        <div id="feedback">
                            <div className="card-header" id="byCEO">
                                Penilaian
                            </div>
                            <div className="card-body">
                                <p id="nilai">
                                <strong>Nilai:</strong> {this.state.submission.score} </p>
                                <p id="feedback-label">
                                <strong>Feedback:</strong></p>
                                <p id="feedback-result"> {this.state.submission.feedback} </p>
                                
                                <button onClick={this.showModal.bind(this, 'modalEditSubmissionElements')}>Ubah Penilaian</button>
                            </div>
                        </div>
                        <FormModal
                            open = {this.state.modalEditSubmissionElements}
                            toggle = {this.closeModal.bind(this, 'modalEditSubmissionElements')}
                            judul = "Ubah Penilaian"
                            >
                            <FormScoring toggle = {this.closeModal.bind(this, 'modalUpdateTask')} toBeUpdatedSubmissionElements = {this.state.submission} />
                        </FormModal>
                    </div>
                </React.Fragment>
            )
        }

        else {
            return (
                <React.Fragment>
                    <div className="current-feedbackContainer">
                        <h3 id="fb-ceo">Dari CEO</h3>
                        <hr id="line-ceo"/>
                        <div id="current-feedback">
                            <div className="card-header" id="byCEO">
                                Hasil Penilaian
                            </div>
                            <div className="card-body">
                                <p id="nilai">
                                <strong>Nilai:</strong> {this.state.submission.score} </p>
                                <p id="feedback-label">
                                <strong>Feedback:</strong></p>
                                <p id="feedback-result"> {this.state.submission.feedback} </p>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            )
        }
    }
}