import React, { Component } from "react";
import Axios from "axios";

import "./TaskCategoryDetail.css";

import Tasks from "components/pages/TaskCategoryDetail/Tasks/Tasks";
import Comments from "components/pages/TaskCategoryDetail/Comments/Comments";
import Feedback from "components/pages/TaskCategoryDetail/Feedback/Feedback";
import Submissions from "components/pages/TaskCategoryDetail/Submissions/Submissions.js";
import Header from "components/base/Header/Header";
import MainSidebar from "components/sidebar/MainSidebar/MainSidebar.js";
import TaskCategoryAttribute from "./TaskCategoryAttribute/TaskCategoryAttribute";


export default class TaskCategoryDetail extends Component {
      constructor(props){
            super(props);
            this.state = {
                  project: {},
                  milestone: {},
                  taskCategory: {},
            }
      }

      async componentDidMount(){
            const response = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/${this.props.match.params.taskCategoryId}/`);
            this.setState({
                  project: response.data.milestone.project,
                  milestone: response.data.milestone,
                  taskCategory: response.data,
            });
            console.log(this.state);
      }

      render(){
            const taskCategoryId = this.props.match.params.taskCategoryId;
            return (
                  <React.Fragment>
                        <div id = "wrapper">
                              <MainSidebar 
                                    active = "projects"
                              />
                              <div id = "content">
                                    <Header 
                                          active = "taskCategoryDetail"
                                          project = {this.state.project}
                                          milestone = {this.state.milestone}
                                          taskCategory = {this.state.taskCategory}
                                    />
                              </div>
                              <TaskCategoryAttribute 
                                    taskCategoryId = {taskCategoryId}
                              />
                              <Tasks 
                                    taskCategoryId = {taskCategoryId}
                              />
                              <Comments 
                                    taskCategoryId = {taskCategoryId}
                              />

                              <Submissions 
                                    taskCategoryId = {taskCategoryId}
                              />

                              <Feedback 
                                    taskCategoryId = {taskCategoryId}
                              />
                        </div>
                  </React.Fragment>
            )
      }
}