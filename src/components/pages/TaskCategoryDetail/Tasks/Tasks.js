import React, { Component } from "react";

import "./Tasks.css";
import Axios from "axios";
import swal from "sweetalert";
import FormModal from "components/forms/Modal/FormModal";
import FormTask from "components/forms/FormTask/FormTask";

export default class Tasks extends Component {
    constructor(props){
        super(props);
        this.state = {
            tasks: [],
            toBeUpdatedTask: null,
            modalCreateTask: false,
            modalUpdateTask: false,
        };
        this.showModal = this.showModal.bind(this);
    };

    closeModal(stateIndex){
        this.setState({
            [stateIndex]: false
        });
    }

    showModal(modal, toBeUpdatedTask){
        this.setState({
            [modal]: true,
        });
        if (toBeUpdatedTask !== null){
            this.setState({
                toBeUpdatedTask
            });
        }
    }

    async componentDidMount(){
        const tasksResponse = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${this.props.taskCategoryId}/tasks`)
        this.setState({
            tasks: tasksResponse.data,
        });
    };

    async removeTaskHandler(event, taskId){
        swal({
            title: "Apakah kamu yakin akan menghapus task ini?",
            text: "Kamu tidak dapat membatalkan penghapusan ini!",
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true,
        })
            .then((willDelete) => {
                if(willDelete) {
                    fetch(`https://ppms-backend-staging.herokuapp.com/api/tasks/${taskId}`, {
                        method: "DELETE",
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json"
                        }
                    })
                        .then(() => {
                            let updatedTasks = [...this.state.tasks].filter(
                                i => i.id !== taskId
                            );
                            this.setState({
                                tasks: updatedTasks,
                            });
                        });
                        swal("Task berhasil dihapus!", {
                            icon: "success",
                        });
                }
                else {
                    swal("Penghapusan task berhasil dibatalkan");
                };
            }); 
    }

    render() {
        if (localStorage.getItem("role") === "Staf Konsultan"){
            return (
                <React.Fragment>
                    <div className = "Task">
                        <h3 id = "todo">To-Do-List</h3>
                        <hr id="linetd"/>
                        <div id="allTask">
                            <div id="btn-plus">    
                                <a onClick = {this.showModal.bind(this, 'modalCreateTask')} id="tambah-task">Tambah Task <a id="plus">+</a></a>
                            </div>

                            <FormModal
                                open = {this.state.modalCreateTask}
                                toggle = {this.closeModal.bind(this, 'modalCreateTask')}
                                judul = "Tambah Task"
                            >
                                <FormTask action = "CREATE" toggle = {this.closeModal.bind(this, 'modalCreateTask')} taskCategoryId = {this.props.taskCategoryId} />
                            </FormModal>

                            <FormModal
                                open = {this.state.modalUpdateTask}
                                toggle = {this.closeModal.bind(this, 'modalUpdateTask')}
                                judul = "Ubah Task"
                            >
                                <FormTask action = "EDIT" toggle = {this.closeModal.bind(this, 'modalUpdateTask')} toBeUpdatedTask = {this.state.toBeUpdatedTask} />
                            </FormModal>

                            {this.state.tasks.map(
                                task => (
                                    <div className = "row">
                                        <input className = "col-md-1" type="checkbox" id="cb" />
                                        <p id="t" className="col md-5">{task.description}</p>    
                                        
                                        <div id="t-det">
                                            <a className="col md-3" onClick = {this.showModal.bind(this, 'modalUpdateTask', task)} id="edit-task">Ubah</a>
                                            <a className="col md-3" onClick = {event => this.removeTaskHandler(event, task.id)} id="hapus-task">Hapus</a>
                                        </div>
                                    </div>
                                )
                            )}
                    </div>

                    </div>
                </React.Fragment>
            )
        }
        else {
            return (
                <React.Fragment>
                    <div className = "Task">
                        <h3 id = "todo">To-Do-List</h3>
                        <hr id="linetd"/>
                        <div id="allTask">
                            {this.state.tasks.map(
                                task => (
                                    <div className = "row">
                                        <input className = "col-md-1" type="checkbox" id="cb" disabled />
                                        <p id="t" className="col md-9">{task.description}</p>    
                                    </div>
                                )
                            )}
                        </div> 
                    </div>
                </React.Fragment>
            )
        }
    }
}