import React, { Component } from 'react';
import './TaskCategoryAttribute.css';
import Axios from 'axios';

export default class TaskCategoryAttribute extends Component {
    constructor(props){
        super(props);
        this.state = {
            taskCategory: [],
            milestone: [],
        }
    }

    async componentDidMount(){
        const taskCategoryResponse = await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/${this.props.taskCategoryId}`)
        this.setState({
            taskCategory: taskCategoryResponse.data,
            milestone: taskCategoryResponse.data.milestone.name,
        });
        console.log(this.state.milestone);
    };

    render() {
        return (
            <React.Fragment>
                <div className="TaskCategoryAttributes">
                    <div className="column" id="bx"></div>
                    <div className="column" id="task">
                        <p id="tc"> {this.state.milestone} - {this.state.taskCategory.name}</p>
                        <hr id="linetc"/>

                        <p id="dt">
                            <b id="due">Due Date:</b>
                            {this.state.taskCategory.dueDate}</p>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
