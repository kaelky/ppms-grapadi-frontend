import React, { Component } from 'react';

import LogoutButton from "assets/Header/Logout.png";
import './Header.css';

import HeaderBreadcrumb from '../HeaderBreadcrumb/HeaderBreadcrumb';
import HeaderPageTitle from '../HeaderPageTitle/HeaderPageTitle';

export default class Header extends Component {
    constructor(props){
        super(props);
        this.state = {
            project: {},
            milestone: {},
            taskCategory: {},
        }
        this.handleLogout = this.handleLogout.bind(this);
    }

    componentDidMount() {
        if (this.props.active === "projectDetail"){
            this.setState({
                project: this.props.project,
            })
        }

        /* else if (this.props.active === "taskCategoryDetail"){
            this.setState({
                project: this.props.project,
                milestone: this.props.milestone,
                taskCategory: this.props.taskCategory,
            });
        } */

    }

    handleLogout = event => {
        window.localStorage.clear();
        window.location.href='/';
    }

    render(){
        return (
            <React.Fragment>
                <nav className = "navbar navbar-expand-lg header-navbar">
                    <div className = "header-container">
                        <HeaderBreadcrumb 
                            currentPage = {this.props.active}
                            project = {this.props.project}
                            milestone = {this.props.milestone}
                            taskCategory = {this.props.taskCategory}
                        />
                        <div className = "row">
                            <div className = "col-lg-4">
                                <HeaderPageTitle 
                                    currentPage = {this.props.active}
                                    project = {this.props.project}
                                />
                            </div>
                            <div className = "col-lg-6">
                                <div className = "row">
                                    <div className = "col-md-3 ml-auto pull-right">
                                        <h4 className = "user-logout">{localStorage.getItem("fullName")}</h4>
                                        <h5 className = "user-logout">{localStorage.getItem("role")}</h5>    
                                    </div>
                                    <div className = "col-md-3">
                                        <img id = "logout-button" src = {LogoutButton} onClick = {this.handleLogout} alt="logout"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </React.Fragment>
        )
    }
}