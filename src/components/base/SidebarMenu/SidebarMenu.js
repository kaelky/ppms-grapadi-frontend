import React from "react";

// import ProjectIcon from "assets/MainSidebar/Project.png";
// import DashboardIcon from "assets/MainSidebar/Dashboard.png";

import "./SidebarMenu.css";

function SidebarMenu(props){
      if ((localStorage.getItem("role") === "Manajer" || localStorage.getItem("role") === "Staf Konsultan")) {
            if (props.active === "events"){
                  return (
                        <div>
                              <div id = "sidebar-menu-container">
                                    {/* <div className = "sidebar-menu-image-container">
                                          <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" />
                                    </div> */}
                                    <div className = "sidebar-menu-description" onClick = {props.changeActiveToProjects}>Daftar Proyek</div>
                              </div>
                              <div id = "sidebar-menu-container">
                                    {/* <div className = "sidebar-menu-image-container">
                                          <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" />
                                    </div> */}
                                    <div className = "sidebar-menu-description-active" onClick = {props.changeActiveToEvents}>Daftar Acara Grapadi</div>
                              </div>
                        </div>
                  )      
            }
            
            else {
                  return (
                        <div>
                              <div id = "sidebar-menu-container">
                                    {/* <div className = "sidebar-menu-image-container">
                                          <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" />
                                    </div> */}
                                    <div className = "sidebar-menu-description-active" onClick = {props.changeActiveToProjects}>Daftar Proyek</div>
                              </div>
                              <div id = "sidebar-menu-container">
                              {/* <div className = "sidebar-menu-image-container">
                                    <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" />
                              </div> */}
                                    <div className = "sidebar-menu-description" onClick = {props.changeActiveToEvents}>Daftar Acara Grapadi</div>
                              </div>
                        </div>
                  )
            }
      }

      else if (localStorage.getItem("role") === "CEO") {
            if (props.active === "dashboard"){
                  return (
                        <div>
                              <div id = "sidebar-menu-container">
                                    {/* <img className = "sidebar-menu-image" src = {DashboardIcon} alt = "logo" id = "logo" /> */}
                                    <span className = "sidebar-menu-description-active" onClick = {props.changeActiveToDashboard}>Dashboard</span>
                              </div>
                              <div id = "sidebar-menu-container">
                                    {/* <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" /> */}
                                    <span className = "sidebar-menu-description" onClick = {props.changeActiveToProjects}>Daftar Proyek</span>
                              </div>
                              <div id = "sidebar-menu-container">
                              {/* <div className = "sidebar-menu-image-container">
                                    <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" />
                              </div> */}
                                    <div className = "sidebar-menu-description" onClick = {props.changeActiveToEvents}>Daftar Acara Grapadi</div>
                              </div>
                        </div>
                  )
            }

            else if (props.active === "projects"){
                  return (
                        <div>
                              <div id = "sidebar-menu-container">
                                    {/* <img className = "sidebar-menu-image" src = {DashboardIcon} alt = "logo" id = "logo" /> */}
                                    <span className = "sidebar-menu-description" onClick = {props.changeActiveToDashboard}>Dashboard</span>
                              </div>
                              <div id = "sidebar-menu-container">
                                    {/* <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" /> */}
                                    <span className = "sidebar-menu-description-active" onClick = {props.changeActiveToProjects}>Daftar Proyek</span>
                              </div>
                              <div id = "sidebar-menu-container">
                              {/* <div className = "sidebar-menu-image-container">
                                    <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" />
                              </div> */}
                                    <div className = "sidebar-menu-description" onClick = {props.changeActiveToEvents}>Daftar Acara Grapadi</div>
                              </div>
                        </div>
                  )
            }

            else if (props.active === "events"){
                  return (
                        <div>
                              <div id = "sidebar-menu-container">
                                    {/* <img className = "sidebar-menu-image" src = {DashboardIcon} alt = "logo" id = "logo" /> */}
                                    <span className = "sidebar-menu-description" onClick = {props.changeActiveToDashboard}>Dashboard</span>
                              </div>
                              <div id = "sidebar-menu-container">
                                    {/* <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" /> */}
                                    <span className = "sidebar-menu-description" onClick = {props.changeActiveToProjects}>Daftar Proyek</span>
                              </div>
                              <div id = "sidebar-menu-container">
                              {/* <div className = "sidebar-menu-image-container">
                                    <img className = "sidebar-menu-image" src = {ProjectIcon} alt = "logo" id = "logo" />
                              </div> */}
                                    <div className = "sidebar-menu-description-active" onClick = {props.changeActiveToEvents}>Daftar Acara Grapadi</div>
                              </div>
                        </div>
                  )
            }
            
      }
}

export default SidebarMenu;