import React from "react";
import { Breadcrumb, BreadcrumbItem } from "reactstrap";

import "./HeaderBreadcrumb.css";

function HeaderBreadcrumb(props) {
    if (props.currentPage === "projects"){
        return (
            <div id = "breadcrumb-container">
                <Breadcrumb>
                    <BreadcrumbItem active><a>Daftar Proyek</a></BreadcrumbItem>
                </Breadcrumb>
            </div>
        )
    }
    else if (props.currentPage === "projectDetail"){
        return (
            <div id = "breadcrumb-container">
                <Breadcrumb>
                    <BreadcrumbItem><a href = "/projects">Daftar Proyek ></a></BreadcrumbItem>
                    <BreadcrumbItem active><a>{props.project.name}</a></BreadcrumbItem>
                </Breadcrumb>
            </div>
        )
    }
    else if (props.currentPage === "taskCategoryDetail"){
        return (
            <div id = "breadcrumb-container">
                <Breadcrumb>
                    <BreadcrumbItem><a href = "/projects">Daftar Proyek ></a></BreadcrumbItem>
                    <BreadcrumbItem><a href = {`/projects/${props.project.id}`}>{props.project.name + " > " + props.milestone.name} > </a></BreadcrumbItem>
                    <BreadcrumbItem active><a>{props.taskCategory.name}</a></BreadcrumbItem>
                </Breadcrumb>
            </div>
        )
    }
    else if (props.currentPage === "dashboard") {
        return (
            <div id = "breadcrumb-container">
                <Breadcrumb>
                    <BreadcrumbItem active><a>Dashboard</a></BreadcrumbItem>
                </Breadcrumb>
            </div>
        )
    }

    else if (props.currentPage === "events"){
        return (
            <div id = "breadcrumb-container">
                <Breadcrumb>
                    <BreadcrumbItem active><a>Daftar Acara</a></BreadcrumbItem>
                </Breadcrumb>
            </div>
        )
    }
}

export default HeaderBreadcrumb;