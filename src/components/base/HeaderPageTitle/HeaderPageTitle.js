import React from "react";

import "./HeaderPageTitle.css";

const HeaderPageTitle = props => {
    if (props.currentPage === "projects"){
        return (
            <div>
                <h2 className = "page-title">Daftar Proyek</h2>
            </div>
        )
    }

    else if (props.currentPage === "dashboard"){
        return (
            <div>
                <h2 className = "page-title">Review Proyek</h2>
            </div>
        )
    }

    else if (props.currentPage === "events"){
        return (
            <div>
                <h2 className = "page-title">Daftar Acara Grapadi</h2>
            </div>
        )
    }

    else{
        return (
            <div>
                <h2 className = "page-title">{props.project.name}</h2>
            </div>
        )
    }
}

export default HeaderPageTitle;