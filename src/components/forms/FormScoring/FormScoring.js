import React, {Component} from "react";
import Axios from "axios";
import swal from "sweetalert";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    FormFeedback,
    FormText,
} from "reactstrap";

import "./FormScoring.css";

export default class FormScoring extends Component {
    constructor(props){
        super(props);
        this.state = {
            score: "",
            feedback: "",
            formErrors: {
                flag: false,
                message: "",
            }
        };
    };

    handleChange = event => {
        const name = event.target.name
        const value = event.target.value;

        if (value === ""){
            this.setState({
                formErrors: {
                    flag: false,
                    message: "Tidak boleh ada bagian yang kosong!",
                }
            })
        }

        else {
            if (name === "score"){
                this.setState({
                    score: value,
                    formErrors: {
                        flag: true,
                        message: "",
                    }
                })
            }
            else if (name === "feedback"){
                this.setState({
                    feedback: value,
                    formErrors: {
                        flag: true,
                        message: "",
                    }
                });
            }
        }
    }

    handleSubmit = event => {

        if (this.state.formErrors.flag === true){
            const updateResponse = fetch(`https://ppms-backend-staging.herokuapp.com/api/submissions/${this.props.toBeUpdatedSubmissionElements.id}`, {
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        
                    },
                    body: JSON.stringify(this.state)
                })
                    .then(response => response.json())

                swal("Nilai dan Feedback berhasil diubah!", {
                    icon: "success"
                })
                    .then(() => {
                        document.location.reload();
                    });
                this.props.toggle();

            this.setState({
                score: "",
                feedback: "",
                formErrors: {
                    flag: false,
                    message: "",
                }
            });
        }

        else {
            swal({
                title: "GAGAL",
                text: "Tidak boleh ada bagian yang kosong!",
                icon: "error"
            })
        }
    }

    render(){
        let tempToBeUpdatedScore = "";
        let tempToBeUpdatedFeedback = "";
        if (this.props.toBeUpdatedSubmissionElements.score !== null){
            tempToBeUpdatedScore = this.props.toBeUpdatedSubmissionElements.score;
        }
        if (this.props.toBeUpdatedSubmissionElements.feedback !== null){
            tempToBeUpdatedFeedback = this.props.toBeUpdatedSubmissionElements.feedback;
        }

        return (
            <React.Fragment>
                <div className = "fb-available">
                    <Label htmlFor="score"><strong>Nilai</strong><span id = "required-star">*</span></Label>
                    <p>Nilai minimal 0 dan maksimal 100</p>
                    <Input
                        onChange = {this.handleChange}
                        // placeholder = {tempToBeUpdatedScore.score}
                        type = "number"
                        min = {0}
                        max = {100}
                        name = "score"
                        placeholder = {tempToBeUpdatedScore}
                    />
                    <br/>
                    <Label htmlFor = "feedback"><strong>Feedback</strong><span id = "required-star">*</span></Label>
                    <Input
                        onChange = {this.handleChange}
                        // placeholder = {tempToBeUpdatedFeedback.feedback}
                        type = "textarea"
                        name = "feedback"
                        placeholder = {tempToBeUpdatedFeedback}
                    />
                    <span id="null-message">{this.state.formErrors.message}</span>
                </div>
                    <Button id = "submit-button" type = "button" onClick = {this.handleSubmit}>Submit</Button>
            </React.Fragment>
        )
    }
}