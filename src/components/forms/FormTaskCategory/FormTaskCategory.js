import React, { Component } from 'react';
import Axios from 'axios';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    FormFeedback,
    FormText,
    Badge,
    Alert
  } from "reactstrap";
  import FilteredMultiSelect from 'react-filtered-multiselect';
  import swal from 'sweetalert';
  const BOOTSTRAP_CLASSES = {
    filter: 'form-control',
    select: 'form-control',
    button: 'btn btn btn-block btn-default',
    buttonActive: 'btn btn btn-block btn-primary',
  }

    var compare_dates = function(date1,date2){
      console.log("---- DATE -----")
      
      var first = new Date(date1);
      var second = new Date(date2);
        if (first>second)
             return (1);
        else if (first<second) 
            return (-1);
        else 
            return (0); 
    }
  
    const emailRegex = RegExp(
      /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    );
    
    const formValid = ({ formErrors, ...rest }) => {
      let valid = true;
    
      // validate form errors being empty
      Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
      });
    
      console.log("COBA UNDEFINED")
      // validate the form was filled out
      Object.values(rest).forEach(val => {
        console.log(val);
        val === null && (valid = false);
  
      });
    
      return valid;
    };

class FormTaskCategory extends Component{
  constructor(props){
    super(props);

    this.state = {
      
         name:null,
         dueDate:null,
         allUsers: [{}],
         users: [],
         updatedDate: '',
         button: '',
         disable: '',
         danger:false,
      
      formErrors:{
         name:'',
         dueDate:'',
         users: {},
      }


    };

    this.getId = this.getId.bind(this);
    this.mySelects = this.mySelects.bind(this);
}

async componentDidMount(){
  
  await Axios
  .get(`https://ppms-backend-staging.herokuapp.com/api/projects/${this.props.proyekId}/users`)
  .then(response => {
    
    const fetchResult = response.data;

    this.setState({
      allUsers: fetchResult
    });
  });

  if(this.props.action == "EDIT"){
      this.setState({
          name: this.props.editTaskCategory.name,
          dueDate:this.props.editTaskCategory.dueDate,
          users:this.props.editTaskCategory.users,
          button:'Ubah Task Category'
      })
  }
  else{
   
    this.setState({
      button: 'Tambah Task Category',
      disable: 'float-right disabled'
    })
  }

  

}

getId(){

  var temp = [];
  
  
  return temp;
}

mySelects(){
// ALL USERS - User 
var temp = [];
this.state.allUsers.forEach(user => {
  if(!this.state.users.includes(user.id)){
    temp.push(user)
  }
  
}); 
this.setState({
allUsers: temp
})   

}

handleSubmit = e => {
  e.preventDefault();
  
  
  
 
  if (formValid(this.state)) {
      
      if(this.props.action == "CREATE"){
          var temp = []
          this.state.users.map(
              user => 
              temp.push(user.id)
              
              
          )

         
          this.setState({users: temp}, function () {
              const response = fetch(`https://ppms-backend-staging.herokuapp.com/api/milestones/${this.props.milestoneId}/taskCategories/`, {
                  method: 'POST',
                  headers: {
                      'Accept' : 'application/json',
                      'Content-Type' : 'application/json',
                      'Access-Control-Allow-Origin' : '*'
                  },
                  body: JSON.stringify(this.state) })
                  .then (response => response.json())
                  
                  swal("Good job!", "Task Category " + this.state.name + " berhasil dibuat", "success")
                  .then(
                    function(){
                      document.location.reload()}
                  )
                 
                  this.props.toggle();    
          });
                      
         }

         if(this.props.action == "EDIT"){
          var temp = []
          this.state.users.map(
              user => 
              temp.push(user.id) 
          )
          
      
         this.setState({users: temp}, function () {
          const response =  fetch(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${this.props.editTaskCategory.id}/`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(this.state)
        })
      swal({
        title:"SUKSES",
        text: "Sukses label berhasil diubah!" ,
        icon: "success",
        button: "Lanjutkan", 
        type:"success"
      }).then(
        function(){
          window.location.reload();
        })
        
    
            this.setState({
              
                  name:'',
                  startDate:'',
                  dueDate:'',
                  users:[],
                  allUsers: [{}],
                  harga:'',
                  user:'',
                  description:''
              ,
              formError:[{
                  projectName:'',
                  startDate:'',
                  dueDate:'',
                  client:'',
                  harga:'',
                  user:'',
                  description:''
              }]
          })

        });
        }
  } else {
     
      this.setState({
        danger: true
    })  
    }
};

handleChange = e => {
  e.preventDefault();
  const { name, value } = e.target;
  let formErrors = { ...this.state.formErrors };
  this.setState({disable:'float-right'})
  switch (name) {
    case "name":
      formErrors.name =
        value.length < 3  ? "minimum 3 characaters required" : "";
      break;
      case "dueDate":
      formErrors.dueDate =
        value.length < 3 ? "minimum 3 characaters required" : "";
      break;
      case "users":
      break;

    default:
      break;
  }

  this.setState({ formErrors, [name]: value }, () => console.log(this.state));
};

handleMemberNameChange = idx => evt => {
    const newMember = this.state.users.map((member, sidx) => {
      if (idx !== sidx) return member;
      return { ...member, id: evt.target.value };
    });

    this.setState({ users: newMember});
  };

 
handleAddMember = () => {
this.setState({
    users: this.state.users.concat([{ }])
});

};

handleRemoveMember = idx => () => {

this.setState({
    users: this.state.users.filter((s, sidx) => idx !== sidx)
});
};


handleDeselect = (deselectedOptions) => {
  var users = this.state.users.slice()
  deselectedOptions.forEach(option => {
    users.splice(users.indexOf(option), 1)
  })
  this.setState({users})
}

handleSelect = (users) => {
  users.sort((a, b) => a.id - b.id)
  this.setState({users})
}

render() {
  const { formErrors } = this.state;
  var {users} = this.state
  

    return(
      <div> 
         <form onSubmit={this.handleSubmit} noValidate>
         {this.state.danger && (
            <Alert color="danger">Harap seluruh informasi diisi dengan benar</Alert>
          )}
        <FormGroup>
          <Label htmlFor="name">Nama Task Category</Label>
          <Input
            className={formErrors.name.length > 0 ? "error" : null}
            placeholder="Nama Task Category"
            type="text"
            name="name"
            noValidate
            onChange={this.handleChange}
            value={this.state.name}
          />
          {formErrors.name.length > 0 && (
            <span className="errorMessage">{formErrors.name}</span>
          )}

      </FormGroup>
        
     
      <FormGroup>
          <Label htmlFor="dueDate">Tenggat Waktu</Label>
          <Input
            className={formErrors.dueDate.length > 0 ? "error" : null}
            placeholder="dueDate"
            type="date"
            name="dueDate"
            noValidate
            onChange={this.handleChange}
            value = {this.state.dueDate}
          />
          {formErrors.dueDate.length > 0 && (
            <span className="errorMessage">{formErrors.dueDate}</span>
          )}
      </FormGroup>
    
      
      

      
      <div className="row">
        <div className="col-md-6">
        <p>Anggota pada proyek:</p>
          <FilteredMultiSelect
            buttonText="Add"
            classNames={BOOTSTRAP_CLASSES}
            onChange={this.handleSelect}
            options={this.state.allUsers}
            selectedOptions={users}
            textProp="fullName"
            valueProp="id"
          />
        </div>
        <div className="col-md-6">
          <p>Anggota yang ditugaskan:</p>
          <FilteredMultiSelect
            buttonText="Remove"
            classNames={{
              filter: 'form-control',
              select: 'form-control',
              button: 'btn btn btn-block btn-default',
              buttonActive: 'btn btn btn-block btn-danger'
            }}
            onChange={this.handleDeselect}
            options={users}
            textProp="fullName"
            valueProp="id"
          />
      </div>
     </div>
     


        
        <br/>
        <div className="createAccount">
          <Button color="primary" className={this.state.disable} type="submit"> {this.state.button}</Button>
          
        </div>
        
        
        
      </form>   

      </div>
    );
}


}

  export default FormTaskCategory;
