import React, { Component } from "react";
import Axios from "axios";
import swal from 'sweetalert';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    FormFeedback,
    FormText
} from "reactstrap";

export default class FormAddLabel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            labelSekarang:'',
            
         
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
      }
    
      async componentDidMount() {
       
          this.setState({
              labelSekarang: this.props.editLabel,
          })
        }
     
    
      async handleSubmit(event) {
        event.preventDefault();
        
        if(this.props.action == "EDIT"){
            const response = await fetch(`https://ppms-backend-staging.herokuapp.com/api/project/currentStage/${this.props.id}/${this.state.labelSekarang}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(this.state)
            })
          swal({
            title: "SUKSES",
            text: "Sukses label berhasil diubah!" ,
            icon: "success",
            button: "Lanjutkan", 
            type:"success"
          }).then(
            function(){
              window.location.reload();}
            )
            this.props.toggle(); 
      
      }
      }
      
        async handleChange(event) {
            this.setState({
              labelSekarang: event.target.value
            });
          }
    
    
    
      render() {
        return (
          <Form>
            <FormGroup>
            <Label for="exampleEmail">Ubah Label</Label>
            <Input
                type="select"
                value={this.state.labelSekarang}
                onChange={this.handleChange}
                name= 'ubahLabel'
                >
                <option value={this.props.editLabel.id}> {this.props.editLabel.name} </option>
                {this.props.labelStatus.map(
                                (labelStatus, index) => 
                                    
                                    <option value={labelStatus.id} key={index}> {labelStatus.name} </option>
                            )}
                </Input>
            
                            
                           
            </FormGroup>
            <Button
              rounded size="sm"
              onClick={this.handleSubmit}
              color="primary"
              className="float-right"
            >
              {" "}
              Submit{" "}
            </Button>
          </Form>
        );
      }
    
}