import React, { Component } from 'react';
import './FormEvent.css';
import swal from 'sweetalert';
import {
    Label,
    Input,
    Button
} from "reactstrap";

var compare_dates = function(date1, date2){
    var first = new Date(date1);
    var second = new Date(date2);
        if(first>second)
            return (1);
        else if(first<second)
            return (-1);
        else
            return (0);
}

const formValid = ({ formErrors, ...rest}) => {
    let valid = true;

    Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });

    Object.values(rest).forEach(val => {
        typeof val == "undefined" && (valid = false);
    });

    return valid;
}

export default class FormEvent extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            date: '',
            time: '',
            author: localStorage.getItem("fullName"),

            formErrors:{
                name: '',
                date: '',
                time: ''
            }
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleTimeChange = this.handleTimeChange.bind(this)
        this.handleDateChange = this.handleDateChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        if (this.props.editEvent!= null) {
            this.setState({
                name: this.props.editEvent.name,
                date: this.props.editEvent.date,
                time: this.props.editEvent.time,
            })
        }
    }

    async handleSubmit(event) {
        event.preventDefault();

        if(formValid(this.state)){

        if (this.props.action === "CREATE") {
            const response = await fetch (`https://ppms-backend-staging.herokuapp.com/api/events/`, {
                method: 'POST',
                headers: {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json',
                    'Access-Control-Allow-Origin' : '*'
                },
                body: JSON.stringify(this.state)
            })
                .then(response => response.json())

                swal("SUKSES", "Acara " + this.state.name + " berhasil dibuat", "success")
                    .then(
                        function(){
                        document.location.reload()}
                    )
                    this.props.togel();

                this.setState({
                    name: '',
                    date: '',
                    time: '',
                })
                
        }

        if (this.props.action === "UPDATE"){
            const response =  fetch(`https://ppms-backend-staging.herokuapp.com/api/events/${this.props.editEvent.id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(this.state)
            })
                .then(response => response.json())
            
                console.log(this.state)
            swal("SUKSES", "Acara "+this.state.name+ " berhasil diubah", "success")
                .then(
                    function(){
                    document.location.reload()}
                )
            this.props.togel();

            this.setState({
                name: '',
                date: '',
                time: '',

                formError:[{
                    name:'',
                    date:'',
                    time:''
                }]
            })
        }
    }
    else {
        console.error("Error");
    }
    };

    handleNameChange(event) {
        const {value }=event.target;
        let formErrors = { ...this.state.formErrors};
        formErrors.name = 
            value.length < 3 ? "Nama acara minimal 3 huruf" : "";
        this.setState({ formErrors, name: value })
    }

    handleDateChange(event) {
        const {value} = event.target;
        let formErrors = { ...this.state.formErrors};
        formErrors.date = 
            compare_dates(value, new Date()) < 0 ? "Tanggal sudah lewat" : "";
        this.setState({ formErrors, date: value})
    }

    handleTimeChange(event) {
        this.setState({time: event.target.value})
    }

    render() {
        const { formErrors } = this.state;

        return (
            <form>
                <div>
                    <Label htmlFor="name">Nama Acara</Label>
                    <Input
                        className={formErrors.name.length > 0 ? "error" : null}
                        placeholder="Nama Acara"
                        type="text"
                        name="name"
                        noValidate
                        value = {this.state.name}
                        onChange={this.handleNameChange}/>
                    
                        {formErrors.name.length > 0 && (
                            <span className="errorMessage">{formErrors.name}</span>
                        )}
                </div>
                <br/>
                <div>
                    <Label htmlFor="date">Tanggal</Label>
                    <Input
                        className={formErrors.date.length > 0 ? "error": null}
                        type="date"
                        name="date"
                        value={this.state.date}
                        onChange={this.handleDateChange}/>

                        {formErrors.date.length > 0 && (
                            <span className="errorMessage">{formErrors.date}</span>
                        )}
                </div>
                <br/>
                <div>
                    <Label htmlFor="time">Waktu</Label>
                    <Input
                        type="time"
                        name="time"
                        value={this.state.time}
                        onChange={this.handleTimeChange}/>
                </div>
                <br/>

                <Button color="primary" className="float-right" type="submit" onClick={this.handleSubmit}>Simpan</Button>
            </form>
        );
    }
}

