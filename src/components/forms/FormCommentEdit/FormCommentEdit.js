import React, { Component } from 'react';
import swal from 'sweetalert';
import './FormCommentEdit.css';

import {
    Button,
    Input,
    Label,
} from "reactstrap";

export default class FormCommentEdit extends Component {
    constructor(props){
        super(props);
        this.state = {
            content: "",
            user: localStorage.getItem("email"),
            formErrors: {
                flag: false,
                message: "",
            }
        };
    };

    handleChange = event => {
        const value = event.target.value;
        
        if (event.target.value === ""){
            this.setState({
                formErrors: {
                    flag: false,
                    message: "Bagian ini tidak boleh kosong!",
                }
            })
        }
        
        else {
            this.setState({
                content: value,
                formErrors: {
                    flag: true,
                    message: "",
                }
            });
        }
    }

    handleSubmit = event => {

        if (this.state.formErrors.flag === true){
            if (this.props.action === "EDIT") {
                const updateCommentResponse = fetch(`https://ppms-backend-staging.herokuapp.com/api/comments/${this.props.toBeUpdatedComment.id}`, {
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'
                    },
                    body: JSON.stringify(this.state)
                })
                    .then(response => response.json())

                swal({
                    title: "SUKSES",
                    text: "Komentar berhasil dibuat",
                    icon: "success",
                    button: "Lanjutkan",
                })
                    .then(() => {
                        document.location.reload();
                    });
                this.props.toggle();
            }

            this.setState({
                content: "",
                formErrors: {
                    content: "",
                }
            });
        }

        else {
            swal({
                title: "GAGAL",
                text: "Komentar tidak boleh kosong!",
                icon: "error"
            })
        }
    }



    render(){
        let tempToBeUpdatedCommentContent = "";
        if (this.props.action === "EDIT"){
            tempToBeUpdatedCommentContent = this.props.toBeUpdatedComment.content;
        }
        return (
            <React.Fragment>
                <div className = "form-wrapper">
                    <div className = "content">
                        <Label htmlFor="content">Komentar<span id = "required-star">*</span></Label>
                        <Input
                            type = "textarea"
                            onChange = {this.handleChange}
                            placeholder = {tempToBeUpdatedCommentContent}
                        />
                        <span id="null-message">{this.state.formErrors.message}</span>

                    </div>
                    <Button id = "submit-button" type = "button" onClick = {this.handleSubmit}>Simpan</Button>
                </div>
            </React.Fragment>
        )
    }
}
