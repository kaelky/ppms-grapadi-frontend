import React, { Component } from 'react';
import Axios from 'axios';
import swal from 'sweetalert';

import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    FormFeedback,
    FormText,
    Alert,

  } from "reactstrap";

import { isNull } from 'util';
 
const formValid = ({ formErrors, ...rest }) => {
    let valid = true;
    
    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });
  
    // validate the form was filled out
    Object.values(rest).forEach(val => {
        val === null && (valid = false);
    });
    return valid;
};
  
export default class FormLabelStatus extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          labelSatuan: null,
          danger:false,
          
          formErrors:{
    
            labelSatuan: '',
          }
        }
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);
      }
      handleSubmit = e => {
        e.preventDefault();
        if (formValid(this.state)) {
            if (this.props.action == "CREATE") {
              const response = fetch(
                `https://ppms-backend-staging.herokuapp.com/api/project/addLabel/${this.props.id}/${
                  this.state.labelSatuan
                }`,
                {
                  method: "POST",
                  headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                  },
                  body: JSON.stringify(this.state)
                }
              ).then(response => response.json());
              swal({
                title: "SUKSES",
                text: " Label Status " + this.state.labelSatuan + " berhasil ditambahkan" ,
                icon: "success",
                button: "Lanjutkan", 
                type:"success"
              }).then(
              function(){ 
                window.location.reload();
              })
              this.props.toggle();
        
        
            }
        } else {
          this.setState({
            danger:true
        })
        }
      };
    
      handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = { ...this.state.formErrors };
    
        switch (name) {
          case "labelSatuan":
            formErrors.labelSatuan =
              value.length < 1 ? "Form harus diisi" : "";
            break;
         
        }
    
        this.setState({ formErrors, [name]: value }, () => console.log(this.state));
      };
    
      render() {
        const { formErrors } = this.state;
    
        return (
          <div className="wrapper">
            <div className="form-wrapper">
        
              <form onSubmit={this.handleSubmit} noValidate>
              {this.state.danger && (
                    <Alert color="danger">Form tidak boleh kosong</Alert>
                  )}
                <div className="labelSatuan">
                  <Label htmlFor="labelSatuan">Nama Label</Label>
                  <Input
                    className={formErrors.labelSatuan.length > 0 ? "error" : null}
                    name="labelSatuan"
                    noValidate
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.labelSatuan}
                    placeholder="Isi Nama Label..."
                    
                     
                  >
                  
              
                  </Input>
                 
                  {formErrors.labelSatuan.length > 0 && (
                    <span className="errorMessage">{formErrors.labelSatuan}</span>
                  )}
  
                </div>
                
                <div className="createAccount">
                <Button 
                      color ="primary"
                       size ="sm"
                       onClick={this.handleSubmit}
                      color="primary"
                      className="float-right">{" "}Submit{" "}
                </Button>
                  
                </div>
              </form>
            </div>
          </div>
        );
      }
  
}