import React, { Component } from "react";
import Axios from "axios";
import swal from 'sweetalert';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    FormFeedback,
    FormText
} from "reactstrap";
import FilteredMultiSelect from 'react-filtered-multiselect';
const BOOTSTRAP_CLASSES = {
    filter: 'form-control',
    select: 'form-control',
    button: 'btn btn btn-block btn-default',
    buttonActive: 'btn btn btn-block btn-primary',
}

export default class FormKomponenProyek extends Component {
    constructor(props) {
        super(props);
        this.state = {
          users: [{}],
          user: [],
          
    
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        
      }
      
    
      async componentDidMount() {
        this.setState({
          user: this.props.registeredUsers
        });
        await Axios.get(`https://ppms-backend-staging.herokuapp.com/api/users/`).then(response => {
          
          var anggotaKeseluruhan=[]
          response.data.forEach(user => {
              
              
              if(!this.props.registeredUsers.includes(user.id)){
                anggotaKeseluruhan.push(user)
              }
             
          });
    
          
    
          this.setState({
            users: anggotaKeseluruhan
          });
    
          
        });
      }
    
      async handleSubmit(event) {
        event.preventDefault();
        
        if (this.props.action == "CREATE") {
          var temp = []
          this.state.user.map(
              userSatuan => 
              temp.push(userSatuan.id)
          )
    
    
          this.setState({users: temp}, function () {
          const response =  fetch(
            `https://ppms-backend-staging.herokuapp.com/api/projects/${this.props.id}/users`,
            {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
              },
              body: JSON.stringify(this.state)
            }
          ).then(response => response.json());
          swal({
            title: "SUKSES",
            text: "Sukses Anggota berhasil diubah" ,
            icon: "success",
            button: "Lanjutkan", 
            type:"success"
          }).then(
            function(){ 
              window.location.reload();
            })
          this.props.toggle();
        });
        }
       
      }
        handleDeselect = (deselectedOptions) => {
          var user = this.state.user.slice()
          deselectedOptions.forEach(option => {
            user.splice(user.indexOf(option), 1)
          })
          this.setState({user})
        }
        handleSelect = (user) => {
          user.sort((a, b) => a.id - b.id)
          this.setState({user})
        }
    
      render() {
       var {user} = this.state
    
        return (
          
          <Form onSubmit={this.handleSubmit}>
            <div className="row">
            <div className="col-md-6">
              <Label className = "label1">Seluruh Karyawan</Label>
              <FilteredMultiSelect
                buttonText="Tambah"
                classNames={BOOTSTRAP_CLASSES}
                onChange={this.handleSelect}
                options={this.state.users}
                selectedOptions={user}
                textProp="fullName"
                valueProp="id"
              />
            </div>
            <div className="col-md-6">
            <Label>Karyawan proyek ini</Label>
              <FilteredMultiSelect
                buttonText="Hapus"
                classNames={{
                  filter: 'form-control',
                  select: 'form-control',
                  button: 'btn btn btn-block btn-default',
                  buttonActive: 'btn btn btn-block btn-danger'
                }}
                onChange={this.handleDeselect}
                options={user}
                textProp="fullName"
                valueProp="id"
              />
          </div>
          
        </div>
    
        <Button color="primary" className="float-right" type="submit">Simpan</Button>
       </ Form>
        );
      }
    
}