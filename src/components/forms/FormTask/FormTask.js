import React, { Component } from 'react';
import Axios from 'axios';
import swal from 'sweetalert';

import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    FormFeedback,
    FormText,
} from "reactstrap";

import './FormTask.css';

export default class FormTask extends Component {
    constructor(props){
        super(props);
        this.state = {
            description: "",
            formErrors: {
                flag: false,
                message: "",
            }
        };
    };

    handleChange = event => {
        const value = event.target.value;
        
        if (event.target.value === ""){
            this.setState({
                formErrors: {
                    flag: false,
                    message: "bagian ini tidak boleh kosong!",
                }
            })
        }
        
        else {
            this.setState({
                description: value,
                formErrors: {
                    flag: true,
                    message: "",
                }
            });
        }
    }

    handleSubmit = event => {

        if (this.state.formErrors.flag === true){
            if (this.props.action === "CREATE") {
                const createTaskResponse = fetch(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${this.props.taskCategoryId}/tasks/`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'
                    },
                    body: JSON.stringify(this.state)
                })
                    .then(response => response.json());

                swal("Task " + this.state.description + " berhasil dibuat!",  {
                    icon: "success"
                })
                    .then(() => {
                        document.location.reload();
                    });
                this.props.toggle();
            }

            else if (this.props.action === "EDIT") {
                const updateTaskResponse = fetch(`https://ppms-backend-staging.herokuapp.com/api/tasks/${this.props.toBeUpdatedTask.id}`, {
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'
                    },
                    body: JSON.stringify(this.state)
                })
                    .then(response => response.json())

                swal("Task " + this.props.toBeUpdatedTask.description + " berhasil diubah!", {
                    icon: "success"
                })
                    .then(() => {
                        document.location.reload();
                    });
                this.props.toggle();
            }

            this.setState({
                description: "",
                formErrors: {
                    description: "",
                }
            });
        }

        else {
            swal("Task description tidak boleh kosong!", {
                icon: "error"
            })
        }
    }



    render(){
        let tempToBeUpdatedTaskDescription = "";
        if (this.props.action === "EDIT"){
            tempToBeUpdatedTaskDescription = this.props.toBeUpdatedTask.description;
        }
        return (
            <React.Fragment>
                <div className = "form-wrapper">
                    <div className = "description">
                        <Label htmlFor="description">Description<span id = "required-star">*</span></Label>
                        <Input
                            onChange = {this.handleChange}
                            placeholder = {tempToBeUpdatedTaskDescription}
                        />
                        <span id="null-message">{this.state.formErrors.message}</span>

                    </div>
                    <button id = "submit-button" type = "button" onClick = {this.handleSubmit}>Submit</button>
                </div>
            </React.Fragment>
        )
    }
}