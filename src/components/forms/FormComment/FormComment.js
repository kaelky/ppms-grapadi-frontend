import React, { Component } from 'react';
import './FormComment.css';
import swal from 'sweetalert';

export default class FormComment extends Component {
    constructor(props){
        super(props);
        this.state = {
            content: "",
            user: localStorage.getItem("email"),
            formErrors: {
                flag: false,
                message: "",
            }
        };
    };

    handleContentChange = event => {
        const value = event.target.value;
        
        if (event.target.value === ""){
            this.setState({
                formErrors: {
                    flag: false,
                    message: "Bagian ini tidak boleh kosong!",
                }
            })
        }
        
        else {
            this.setState({
                content: value,
                formErrors: {
                    flag: true,
                    message: "",
                }
            });
        }
    }

    handleSubmit = event => {

        if (this.state.formErrors.flag === true){
            if (this.props.action === "CREATE") {
                const updateCommentResponse = fetch(`https://ppms-backend-staging.herokuapp.com/api/taskCategories/${this.props.taskCategoryId}/comments`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'
                    },
                    body: JSON.stringify(this.state)
                })
                    .then(response => response.json())

                swal({
                    title: "SUKSES",
                    text: "Komentar berhasil dibuat",
                    icon: "success",
                    button: "Lanjutkan",
                })
                    .then(() => {
                        document.location.reload();
                    });
            }

            this.setState({
                content: "",
                formErrors: {
                    content: "",
                }
            });
        }

        else {
            swal({
                title: "GAGAL",
                text: "Komentar tidak boleh kosong!",
                icon: "error"
            })
        }
    }

    render() {
        return(
            <React.Fragment>
                <div className="Review">
                    <h3 id="mnj">Dari Manajer</h3>
                    <hr id="linemnj"/>
                    <div className="my-card">
                        <form>
                            <div id="comment-box">
                                <textarea 
                                    name = "content" 
                                    id = "comments" 
                                    placeholder = "Tulis komentar Anda disini..." 
                                    onChange = {this.handleContentChange}
                                />
                                <span id="null-message-create">{this.state.formErrors.message}</span>
                                <button type = "button" onClick = {this.handleSubmit}>Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
