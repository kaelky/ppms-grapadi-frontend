import React from "react";

import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col
} from "reactstrap";

const FormModal = ({ toggle, open, children, judul }) => {
    return (
        <Modal isOpen={open} toggle={toggle}>
            <ModalHeader toggle={toggle}>
                <div className="modal-title">
                    {judul}
                </div>
            </ModalHeader>
            <ModalBody>
                {children}
            </ModalBody>
        </Modal>
    );
  };
  
  export default FormModal;
  