import React, { Component } from 'react';
import Axios from 'axios';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    FormFeedback,
    FormText,
    Badge,
    Alert,
  } from "reactstrap";
  import './FormErrorMilestone.css';
  import swal from 'sweetalert';

  var compare_dates = function(date1,date2){
    console.log("---- DATE -----")
    
    var first = new Date(date1);
    var second = new Date(date2);
      if (first>second)
           return (1);
      else if (first<second) 
          return (-1);
      else 
          return (0); 
  }

  const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  );
  
  const formValid = ({ formErrors, ...rest }) => {
    let valid = true;
  
    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });
  
    // validate the form was filled out
    Object.values(rest).forEach(val => {
      val === null && (valid = false);

    });
  
    return valid;
  };




class FormMilestone extends Component {
  constructor(props) {
    super(props);
    this.state = {
        name: null,
        startDate: null,
        endDate: null,
        danger: false,
        
    
    formErrors:{
        name: '',
        startDate: '',
        endDate: '',

    }
    
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    console.log(this.state.name)   
 }
 


handleSubmit = e => {
    e.preventDefault();
    


if (formValid(this.state)) {
console.log(`
  --SUBMITTING--
  First Name: ${this.state.firstName}
  Last Name: ${this.state.lastName}
  Email: ${this.state.email}
  Password: ${this.state.password}
`);

if (this.props.action === "CREATE"){
    console.log("masoklagi")
    const response =  fetch(`https://ppms-backend-staging.herokuapp.com/api/${this.props.id}/milestones/`, {
    method: 'POST',
    headers: {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json',
        'Access-Control-Allow-Origin' : '*'
    },
    body: JSON.stringify(this.state)
}).
then(response => response.json());
swal({
  title: "SUKSES",
  text: " Milestone " + this.state.name + " berhasil tambahkan" ,
  icon: "success",
  button: "Lanjutkan", 
  type:"success"
}).then(
function(){ 
  window.location.reload();
})

console.log(response);
this.props.toggle();
   
   
}



} else {
console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
this.setState({
  danger:true
})
}
};

handleChange = e => {
e.preventDefault();
const { name, value } = e.target;
let formErrors = { ...this.state.formErrors };

switch (name) {
  case "name":
    formErrors.name =
      value.length < 3 ? "Diharuskan minimal 3 huruf" : "";
    break;
  case "startDate":
    formErrors.startDate =
      compare_dates(value,new Date()) < 0 ? "Hari yang dipilih telah berlalu" : "";
    break;
  case "endDate":
    formErrors.endDate =
      compare_dates(value,this.state.startDate) < 0 ? "Harus setelah waktu mulai" : "";
    break;
}

this.setState({ formErrors, [name]: value }, () => console.log(this.state));
};




render() {
    const { formErrors } = this.state;
    return (
        <div className="wrapper">
      <div className="form-wrapper">
        <form onSubmit={this.handleSubmit} noValidate>
        {this.state.danger && (
              <Alert color="danger">Harap seluruh informasi diisi dengan benar</Alert>
            )}
        <div className="name">
            <Label htmlFor="name">Nama Milestone</Label>
            <Input
              className={formErrors.name.length > 0 ? "error" : null}
              placeholder="Nama Milestone"
              type="text"
              name="name"
              noValidate
              onChange={this.handleChange}
              value={this.state.name}
            />
            {formErrors.name.length > 0 && (
              <span className="errorMessage">{formErrors.name}</span>
            )}
        </div>

    <div className="startDate">
      <Label htmlFor="startDate">Waktu Mulai</Label>
      <Input
        className={formErrors.startDate.length > 0 ? "error" : null}
        
        type="date"
        name="startDate"
        noValidate
        onChange={this.handleChange}
        value={this.state.startDate}
      />
      {formErrors.startDate.length > 0 && (
        <span className="errorMessage">{formErrors.startDate}</span>
      )}
    </div>
    <div className="endDate">
      <Label htmlFor="endDate">Tenggat Waktu</Label>
      <Input
        className={formErrors.endDate.length > 0 ? "error" : null}
        placeholder="endDate"
        type="date"
        name="endDate"
        noValidate
        onChange={this.handleChange}
        value = {this.state.endDate}
      />
      {formErrors.endDate.length > 0 && (
        <span className="errorMessage">{formErrors.endDate}</span>
      )}
    </div>
      
<Button
      rounded size="sm"
      onClick={this.handleSubmit}
      color="primary"
      className="float-right"
    >
      {" "}
      Submit{" "}
    </Button>

    </form>
</div>
</div>








    );

}

}

export default FormMilestone;
