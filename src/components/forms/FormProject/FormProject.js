import React, { Component } from 'react';
import Axios from 'axios';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    FormFeedback,
    FormText,
    Alert
} from "reactstrap";
import './FormProject.css';
import swal from 'sweetalert';

var compare_dates = function(date1,date2){
var first = new Date(date1);
var second = new Date(date2);
    if (first>second)
        return (1);
    else if (first<second) 
        return (-1);
    else 
        return (0); 
}

const emailRegex = RegExp(
/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);
  
const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });

    // validate the form was filled out
    Object.values(rest).forEach(val => {
        val === null && (valid = false);
    });

    return valid;
};
  
export default class FormProject extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          
              projectName:null,
              startDate:null,
              dueDate:null,
              client:null,
              harga:null,
              user:'',
              description:null,
              users:[],
              button:'',
              danger:false,
          
          formErrors:{
              projectName:'',
              startDate:'',
              dueDate:'',
              client:'',
              harga:'',
              user:'',
              description:''
          }
  
  
        }
      }
      
  
      componentDidMount() {
        console.log("-------DID MOUNT ----------")
          Axios
              .get(`https://ppms-backend-staging.herokuapp.com/api/users/`)
              .then(response => {
              
              const fetchResult = response.data;
              this.setState({
                  users: fetchResult
              });
          }).then(console.log(this.state.users)
          );
          
          
  
          if(this.props.editProject!=null){
              this.setState({
                  projectName: this.props.editProject.name,
                  startDate:this.props.editProject.startDate,
                  dueDate:this.props.editProject.endDate,
                  client:this.props.editProject.client,
                  harga: this.props.editProject.value,
                  user:this.props.editProject.picuser.id,
                  description:this.props.editProject.description,
                  button:'Ubah Proyek'
              })
          }
          else{
            this.setState({
              button: 'Tambah Proyek'
            })
          }
  
          
      }
  
      handleSubmit = e => {
        e.preventDefault();
    
        if (formValid(this.state)) {
          console.log(`
            --SUBMITTING--
            First Name: ${this.state.firstName}
            Last Name: ${this.state.lastName}
            Email: ${this.state.email}
            Password: ${this.state.password}
          `);
          
          if(this.props.action == "CREATE"){
            
            const response = fetch('https://ppms-backend-staging.herokuapp.com/api/projects/', {
                method: 'POST',
                headers: {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json',
                    'Access-Control-Allow-Origin' : '*'
                },
                body: JSON.stringify(this.state) })
                .then (response => response.json())
                
                swal("Sukses!", "Proyek " + this.state.projectName + " berhasil dibuat", "success")
                .then(
                  function(){
                    document.location.reload()}
                )
                
                this.props.toggle();              
           }
  
           if(this.props.action == "EDIT"){
           
            const response =  fetch(`https://ppms-backend-staging.herokuapp.com/api/projects/${this.props.editProject.id}`, {
              method: 'PUT',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
              },
              body: JSON.stringify(this.state)
          })
              .then(response => response.json())
              
              swal("Sukses!", "Proyek " + this.state.projectName + " berhasil diubah", "success")
              .then(
                function(){
                  document.location.reload()}
              )
              console.log(response);
              this.props.toggle();    
              this.setState({
                
                    projectName:'',
                    startDate:'',
                    dueDate:'',
                    client:'',
                    harga:'',
                    user:'',
                    description:''
                ,
                formError:{
                    projectName:'',
                    startDate:'',
                    dueDate:'',
                    client:'',
                    harga:'',
                    user:'',
                    description:''
                }
            })
          }
          
           
  
  
        } else {
          
          this.setState({
            danger:true
        })
        }
      };
    
      handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = { ...this.state.formErrors };
    
        switch (name) {
          case "projectName":
            formErrors.projectName =
              value.length < 3 ? "minimum 3 characaters required" : "";
            break;
          case "startDate":
            formErrors.startDate =
              compare_dates(value,new Date()) < 0 ? "Hari yang dipilih telah berlalu" : "";
            break;
          case "dueDate":
            formErrors.dueDate =
              compare_dates(value,this.state.startDate) < 0 ? "Harus setelah waktu mulai" : "";
            break;
          case "client":
            formErrors.client =
              value.length < 6 ? "minimum 6 characaters required" : "";
            break;
          case "harga":
            formErrors.harga =
            value.length < 6 ? "minimum 6 characaters required" : "";
          break;
          case "user":
            formErrors.user =
            value == null ? "minimum 6 characaters required" : "";
          break;
          case "description":
            formErrors.description =
            value.length < 6 ? "minimum 6 characaters required" : "";
          break;
          default:
            break;
        }
    
        this.setState({ formErrors, [name]: value }, () => console.log(this.state));
      };
    
      render() {
        const { formErrors } = this.state;
    
        return (
          <div className="wrapper">
            <div className="form-wrapper">
        
              <form onSubmit={this.handleSubmit} noValidate>
              {this.state.danger && (
                    <Alert color="danger">Harap seluruh informasi diisi dengan benar</Alert>
                  )}
                <div className="projectName">
                  <Label htmlFor="projectName">Project Name</Label>
                  <Input
                    className={formErrors.projectName.length > 0 ? "error" : null}
                    placeholder="Project Name"
                    type="text"
                    name="projectName"
                    noValidate
                    onChange={this.handleChange}
                    value={this.state.projectName}
                  />
                  {formErrors.projectName.length > 0 && (
                    <span className="errorMessage">{formErrors.projectName}</span>
                  )}
  
                </div>
                
                <Row form>
                    <Col md={6}>
  
                <div className="startDate">
                  <Label htmlFor="startDate">Start Date</Label>
                  <Input
                    className={formErrors.startDate.length > 0 ? "error" : null}
                    
                    type="date"
                    name="startDate"
                    noValidate
                    onChange={this.handleChange}
                    value={this.state.startDate}
                  />
                  {formErrors.startDate.length > 0 && (
                    <span className="errorMessage">{formErrors.startDate}</span>
                  )}
                </div>
                  </ Col>
                  < Col md={6}>
                <div className="dueDate">
                  <Label htmlFor="dueDate">Tenggat Waktu</Label>
                  <Input
                    className={formErrors.dueDate.length > 0 ? "error" : null}
                    placeholder="dueDate"
                    type="date"
                    name="dueDate"
                    noValidate
                    onChange={this.handleChange}
                    value = {this.state.dueDate}
                  />
                  {formErrors.dueDate.length > 0 && (
                    <span className="errorMessage">{formErrors.dueDate}</span>
                  )}
                </div>
                  </Col>
              </ Row>
                
                <div className="client">
                  <Label htmlFor="client">Nama Klien</Label>
                  <Input
                    className={formErrors.client.length > 0 ? "error" : null}
                    placeholder="client"
                    type="text"
                    name="client"
                    noValidate
                    onChange={this.handleChange}
                    value={this.state.client}
                  />
                  {formErrors.client.length > 0 && (
                    <span className="errorMessage">{formErrors.client}</span>
                  )}
                </div>
                <div className="harga">
                  <Label htmlFor="harga">Harga proyek</Label>
                  <Input
                    className={formErrors.harga.length > 0 ? "error" : null}
                    placeholder="harga"
                    type="text"
                    name="harga"
                    noValidate
                    onChange={this.handleChange}
                    value={this.state.harga}
                  />
                  {formErrors.harga.length > 0 && (
                    <span className="errorMessage">{formErrors.harga}</span>
                  )}
                </div>
                <div className="user">
                  <Label htmlFor="user">Project Manager</Label>
                  <Input
                    className={formErrors.user.length > 0 ? "error" : null}
                    placeholder="Project Manager"
                    type="select"
                    name="user"
                    noValidate
                    onChange={this.handleChange}
                    value={this.state.user}
                  >
                  <option selected>Project Manager</option> 
                  {this.state.users.map(user => (
                  <option value={user.id}> {user.fullName} </ option>  
                  ))}
  
      
                  </Input>
  
  
                  {formErrors.user.length > 0 && (
                    <span className="errorMessage">{formErrors.user}</span>
                  )}
                </div>
                <div className="description">
                  <Label htmlFor="description">Deskripsi</Label>
                  <Input
                    className={formErrors.description.length > 0 ? "error" : null}
                    placeholder="description"
                    type="textarea"
                    name="description"
                    noValidate
                    onChange={this.handleChange}
                    value={this.state.description}
                  />
                  {formErrors.description.length > 0 && (
                    <span className="errorMessage">{formErrors.description}</span>
                  )}  
                </div>
                <br/>
                <div className="createAccount">
                  <Button color="primary" className="float-right" type="submit">{this.state.button}</Button>
                  
                </div>
              </form>
            </div>
          </div>
        );
      }
  
}