import React, { Component } from "react";
import ReactFC from 'react-fusioncharts';
import FusionCharts from 'fusioncharts';
import Column2D from 'fusioncharts/fusioncharts.charts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import "./PerformanceChart.css";

ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);

export default class PerformanceChart extends Component {
    constructor(props){
        super(props);
        this.state = {

        };
    };

    render() {
        const data1 = 
            this.props.projectPerformanceChartSummaries.map(
                projectPerformanceSummary => (
                    {
                        "label": projectPerformanceSummary.user.fullName,
                        "value": projectPerformanceSummary.averageOfTaskCategoriesScore,
                    }
                )
            );

        const chartConfigs = {
            type: 'column2d',// The chart type
            width: '900', // Width of the chart
            height: '300', // Height of the chart
            dataFormat: 'json', // Data type
            dataSource: {
                // Chart Configuration
                "chart": {
                    "caption": this.props.chosenProject.name,
                    "subCaption": this.props.chosenProject.description,
                    "xAxisName": "Staf Konsultan",
                    "yAxisName": "Rata - Rata Nilai",
                    "theme": "fusion",
                },
                // Chart Data
                "data": data1
                // "data": [{
                //     "label": "Venezuela",
                //     "value": "290"
                // }, {
                //     "label": "Saudi",
                //     "value": "260"
                // }, {
                //     "label": "Canada",
                //     "value": "180"
                // }, {
                //     "label": "Iran",
                //     "value": "140"
                // }, {
                //     "label": "Russia",
                //     "value": "115"
                // }, {
                //     "label": "UAE",
                //     "value": "100"
                // }, {
                //     "label": "US",
                //     "value": "30"
                // }, {
                //     "label": "China",
                //     "value": "30"
                // }]
            }
        };

        return (
            <React.Fragment>
                <div className = "chart-container">
                    <div className = "project-title">
                        {this.props.chosenProject.name}
                    </div>
                    <div className = "chart">
                        <ReactFC {...chartConfigs}/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}