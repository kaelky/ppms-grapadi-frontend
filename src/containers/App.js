import React, { Component } from "react";
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

//Import All Pages' Components Required

import "./App.css";
import Projects from "components/pages/Projects/Projects.js";
import Performance from "components/pages/Performance/Performance";
import Event from "components/pages/Event/Events.js";
import ProjectDetail from "components/pages/ProjectDetail/ProjectDetail.js";
import ProjectUser from "components/pages/ProjectDetail/ProjectUser/ProjectUser";
import TaskCategoryDetail from "components/pages/TaskCategoryDetail/TaskCategoryDetail.js";
import Login from "components/pages/Login/Login.js";
import NotFound from "components/base/NotFound/NotFound";
import Register from "components/pages/Register/Register";
import Admin from "components/pages/Admin/Admin";

//Import All Components Required

export default class App extends Component {
  render() {
    const role = localStorage.getItem("role");
    if (localStorage.getItem("isLoggedOn") === "true") {
      if (role === "CEO") {
        return (
          <BrowserRouter>
            <Switch>
            <Redirect exact from='/' to='/projects' />
              <Redirect exact from='/login' to='/projects' />
              <Route exact path = '/projects' component = {Projects} />
              <Route exact path = '/projects/:id' component = {ProjectDetail} />
              <Route exact path = '/projects/:id/users' component = {ProjectUser} />
              {/* <Route exact path = '/projects/:projectId/taskCategories/:taskCategoryId' component = {TaskCategoryDetail} /> */}
              <Route exact path = '/taskCategories/:taskCategoryId' component = {TaskCategoryDetail} />
              <Route exact path = '/performance' component = {Performance} />
              <Route exact path = '/performance/:projectId' component = {Performance} />
              <Route exact path = '/events/' component = {Event} />
              <Route exact path = '/notfound' component = {NotFound} />
              <Route exact path = '/register' component = {Register} />
              <Route exact path = '/admin/:table' component = {Admin} />
              <Redirect to = '/notfound'/>
            </Switch>
          </BrowserRouter>
        )
      }
      else if (role === "Manajer") {
        return (
          <BrowserRouter>
            <Switch>
              <Redirect exact from='/' to='/projects' />
              <Redirect exact from='/login' to='/projects' />
              <Route exact path = '/projects' component = {Projects} />
              <Route exact path = '/projects/:id' component = {ProjectDetail} />
              <Route exact path = '/projects/:id/users' component = {ProjectUser} />
              {/* <Route exact path = '/projects/:projectId/taskCategories/:taskCategoryId' component = {TaskCategoryDetail} /> */}
              <Route exact path = '/taskCategories/:taskCategoryId' component = {TaskCategoryDetail} />
              <Route exact path = '/events/' component = {Event} />
              <Route exact path = '/notfound' component = {NotFound} />
              <Redirect to = '/notfound'/>
            </Switch>
          </BrowserRouter>
        )
      }

      else if (role === "Staf Konsultan") {
        return (
          <BrowserRouter>
            <Switch>
              <Redirect exact from='/' to='/projects' />
              <Redirect exact from='/login' to='/projects' />
              <Route exact path = '/projects' component = {Projects} />
              <Route exact path = '/projects/:id' component = {ProjectDetail} />
              <Route exact path = '/projects/:id/users' component = {ProjectUser} />
              {/* <Route exact path = '/projects/:projectId/taskCategories/:taskCategoryId' component = {TaskCategoryDetail} /> */}
              <Route exact path = '/taskCategories/:taskCategoryId' component = {TaskCategoryDetail} />
              <Route exact path = '/events/' component = {Event} />
              <Route exact path = '/notfound' component = {NotFound} />
              <Redirect to = '/notfound'/>
            </Switch>
          </BrowserRouter>
        )
      }
    }

    else {
      return (
        <BrowserRouter>
          <Switch>
            <Redirect exact from='/' to='/login' />
					  <Route exact path='/login' component={Login}/>
            <Route exact path = '/notfound' component = {NotFound} />
            <Redirect to = '/notfound'/>
          </Switch>
        </BrowserRouter>
      )
    }
  }
}
